package com.zkteco.android.activity;


import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zkteco.android.R;
import com.zkteco.android.tool.ZKLog;

public class BaseAboutActivity extends Activity {
    public static final String TAG = BaseAboutActivity.class.getName();
    private TextView textViewVersion;
    private TextView textViewCopyRight;
    private ImageButton imgBtnBack;

    private TextView mAboutTitle;
    private TextView mAboutContent;
    private ImageView mAboutIcon;
    private RelativeLayout mActionBarBg;
    private LinearLayout mAboutContentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_about);

        postRegisterApp();
        setVersion();
        setCopyrightText();
    }

    private void postRegisterApp() {

        mAboutTitle = (TextView) findViewById(R.id.about_title);
        mAboutContent = (TextView) findViewById(R.id.about_content);
        mAboutIcon = (ImageView) findViewById(R.id.about_icon);
        textViewVersion = (TextView) findViewById(R.id.about_tv_version);
        textViewCopyRight = (TextView) findViewById(R.id.about_tv_copyRight);
        mActionBarBg = (RelativeLayout) findViewById(R.id.about_actionbar);
        mAboutContentLayout = (LinearLayout) findViewById(R.id.about_content_layout);

        imgBtnBack = (ImageButton) findViewById(R.id.about_btn_back);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setVersion() {

        PackageInfo packageInfo;
        try {
            packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            textViewVersion.setText(" " + packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            ZKLog.e(TAG, Log.getStackTraceString(e));
        }
    }


    private void setCopyrightText() {

        Time time = new Time();
        time.setToNow();
        textViewCopyRight.setText("Copyright©" + time.year + " ZKTeco.All Right Reserved");
    }

    public void setAboutTitle(int aboutTitle, int colorResId) {

        this.mAboutTitle.setText(aboutTitle);
        this.mAboutTitle.setTextColor(getResources().getColor(colorResId));

    }

    public void setTitleColor(int resId) {
        this.mAboutTitle.setTextColor(getResources().getColor(resId));
    }

    public void setAboutTitle(int aboutTitle) {

        this.mAboutTitle.setText(aboutTitle);

    }

    public void setAboutContent(int aboutContent) {

        this.mAboutContent.setText(aboutContent);
    }

    public void setAboutIcon(int aboutIcon) {

        this.mAboutIcon.setBackgroundResource(aboutIcon);
    }

    public void setAboutActionBarBg(int actionBarBg) {
        this.mActionBarBg.setBackgroundResource(actionBarBg);
    }

}

