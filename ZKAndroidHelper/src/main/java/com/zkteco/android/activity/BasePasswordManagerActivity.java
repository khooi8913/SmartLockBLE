package com.zkteco.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.zkteco.android.R;
import com.zkteco.android.commonInterface.OperateCallback;
import com.zkteco.android.dialog.ZkDialogManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BasePasswordManagerActivity extends Activity implements TextWatcher {
    private static final String TAG = BasePasswordManagerActivity.class.getCanonicalName();
    public static final int ACTION_SET_PASSWORD = 1;
    public static final int ACTION_CHECK_PASSWORD = 2;
    public static final int ACTION_RESET_PASSWORD = 3;     //需要输入旧密码的重设密码
    public static final int ACTION_LOGIN = 4;
    public static final int ACTION_TURN_ON_VERIFY = 5;
    public static final String KEY_ACTION = "action";

    public static final int TEXT_WRONG_PASSWORD = 10;
    public static final int TEXT_RE_INPUT_PASSWORD = 11;
    public static final int TEXT_INPUT_PASSWORD = 12;

    private boolean isAuthenticationSuccess;


    private RelativeLayout actionbar;

    private EditText passWordHide;
    private TextView textTitle;
    private LinearLayout linearlayout;
    private TextView passView1;
    private TextView passView2;
    private TextView passView3;
    private TextView passView4;
    private TextView textViewPrompt;
    private TextView textViewPasswordMatchState;

    private Intent intent;
    private InputMethodManager imm;
    private int actionCode;
    private int testCount = 0;
    private String passwordTemp = "";

    private ImageButton mBackBtn;

    /**
     * please overwrite this method in your password manager
     * change BasePasswordManagerActivity to yourself activity
     *
     * @param context  context
     * @param action   public static final int ACTION_SET_PASSWORD = 1;
     *                 public static final int ACTION_CHECK_PASSWORD = 2;
     *                 public static final int ACTION_RESET_PASSWORD = 3;     //需要输入旧密码的重设密码
     *                 public static final int ACTION_LOGIN = 4;
     * @param listener the callback when this activity been finish will be call
     */
    public static void activityStart(Context context, int action,
                                     PasswordManagerListener listener) {
        Intent intent = new Intent(context, BasePasswordManagerActivity.class);
        intent.putExtra(KEY_ACTION, action);
        BasePasswordManagerActivity.setPasswordManagerListener(listener);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        intent = getIntent();

        initMySharedPreferencesManager(this);

        boolean isNeedVerify = MySharedPreferencesManager.isNeedVerify();
        actionCode = intent.getIntExtra(KEY_ACTION, 0);
//        //如果是空密码,则直接设置新密码
        if (actionCode == ACTION_RESET_PASSWORD && MySharedPreferencesManager.isNullPassword()) {
            actionCode = ACTION_SET_PASSWORD;
        }
        isAuthenticationSuccess = false;
        //如果是登入,则先判断是否需要输入密码
        if (!isNeedVerify && actionCode == ACTION_LOGIN) {
            isAuthenticationSuccess = true;
            callbackAndFinish();
        }

        setContentView(R.layout.activity_password);
        initView();

        //修改标题
        if (actionCode != ACTION_LOGIN) {
            textTitle.setText(R.string.setPassword);
        }

        updateShow(actionCode);

        linearlayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                imm.showSoftInput(passWordHide, InputMethodManager.SHOW_FORCED);
            }
        });
    }

    private void initView() {
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        passWordHide = (EditText) findViewById(R.id.one);
        passWordHide.addTextChangedListener(this);

        actionbar = (RelativeLayout) findViewById(R.id.ll_passwordTitle);
        textTitle = (TextView) findViewById(R.id.tv_passwordTitle);

        passView1 = (TextView) findViewById(R.id.password_1);
        passView2 = (TextView) findViewById(R.id.password_2);
        passView3 = (TextView) findViewById(R.id.password_3);
        passView4 = (TextView) findViewById(R.id.password_4);

        linearlayout = (LinearLayout) findViewById(R.id.ll_passwordView);
        textViewPrompt = (TextView) findViewById(R.id.tv_passwordPrompt);

        textViewPasswordMatchState = (TextView) findViewById(R.id.tv_passwordMatchState);
        mBackBtn = (ImageButton) findViewById(R.id.back_btn);
    }

    private void updateShow(int actionCode) {
        switch (actionCode) {
            case ACTION_RESET_PASSWORD:
                textViewPrompt.setText(R.string.inputOldPassword);
                break;

            case ACTION_SET_PASSWORD:
                textViewPrompt.setText(R.string.inputNewPassword);
                break;

            case ACTION_CHECK_PASSWORD:
            case ACTION_LOGIN:
                textViewPrompt.setText(R.string.InputAppPassword);
                setBackBtnVisibility(View.INVISIBLE);
                break;
        }
    }

    private void cleanPassword(int textCode) {
        passWordHide.setText("");
        switch (textCode) {
            case TEXT_WRONG_PASSWORD:
                textViewPrompt.setText(R.string.wrongPassword);
                break;
            case TEXT_RE_INPUT_PASSWORD:
                textViewPrompt.setText(R.string.confirmNewPassword);
                break;
            case TEXT_INPUT_PASSWORD:
                textViewPrompt.setText(R.string.inputNewPassword);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String password = s.toString();
        int length = password.length();

        passView1.setText(length >= 1 ? "*" : "");
        passView2.setText(length >= 2 ? "*" : "");
        passView3.setText(length >= 3 ? "*" : "");
        passView4.setText(length >= 4 ? "*" : "");


        if (length == 4) {
            textViewPasswordMatchState.setText("");
            testCount++;
            switch (actionCode) {
                //确认密码
                case ACTION_CHECK_PASSWORD:
                    if (MySharedPreferencesManager.checkPassword(password)) {
                        isAuthenticationSuccess = true;
                        callbackAndFinish();
                    } else cleanPassword(TEXT_WRONG_PASSWORD);
                    if (testCount >= 2) {
                        isAuthenticationSuccess = false;
                        callbackAndFinish();
                    }
                    break;
                //有密码情况下,需先输入旧密码
                case ACTION_RESET_PASSWORD:
                    if (MySharedPreferencesManager.checkPassword(password)) {
                        actionCode = ACTION_SET_PASSWORD;
                        cleanPassword(TEXT_INPUT_PASSWORD);
                        testCount = 0;
                    } else {
                        cleanPassword(TEXT_WRONG_PASSWORD);
                        testCount = 0;
                    }
//                    if (testCount >= 2) {
//                        isAuthenticationSuccess = false;
//                        callbackAndFinish();
//                    }
                    break;
                //设置密码
                case ACTION_SET_PASSWORD:
                    if (testCount == 1) {
                        passwordTemp = password;
                        cleanPassword(TEXT_RE_INPUT_PASSWORD);
                    } else if (password.equals(passwordTemp)) {
                        MySharedPreferencesManager.savePassword(password);
                        isAuthenticationSuccess = true;
                        callbackAndFinish();
                    } else {
                        cleanPassword(TEXT_INPUT_PASSWORD);
                        textViewPasswordMatchState.setText(R.string.passwordMatchFail);
                        testCount = 0;
                    }
                    break;
                //登入时输入密码
                case ACTION_LOGIN:
                    if (MySharedPreferencesManager.checkPassword(password)) {
//                        imm.hideSoftInputFromWindow(passWordHide.getWindowToken(), 0);
                        isAuthenticationSuccess = true;
                        callbackAndFinish();
                    } else cleanPassword(TEXT_WRONG_PASSWORD);
            }
        }

    }

    private void callbackAndFinish() {
        transferListener();
        mPasswordManagerListener = null;
        if (null != passWordHide) {
            imm.hideSoftInputFromWindow(passWordHide.getWindowToken(), 0);
//            imm.hide
        }
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            callbackAndFinish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void transferListener() {
        if (mPasswordManagerListener != null) {
            mPasswordManagerListener.authenticationComplete(isAuthenticationSuccess);
        }
    }

    private static PasswordManagerListener mPasswordManagerListener;

    public static void setPasswordManagerListener(PasswordManagerListener passwordManagerListener) {
        mPasswordManagerListener = passwordManagerListener;
    }

    public interface PasswordManagerListener {
        void authenticationComplete(Boolean isAuthenticationSuccess);
    }

    public static boolean isNeedVerify() {
        return MySharedPreferencesManager.isNeedVerify();
    }

    public static void setNeedVerify(boolean isNeedVerify) {
        MySharedPreferencesManager.setNeedVerify(isNeedVerify);
    }

    public static boolean isNullPassword() {
        return MySharedPreferencesManager.isNullPassword();
    }

    public static void cleanPassword() {
        MySharedPreferencesManager.savePassword("");
    }

    private static class MySharedPreferencesManager {
        private static SharedPreferences sp;
        private String KEY_PASSWORD_DATA = "app_data";
        public static final String KEY_APP_PASSWORD = "app_password";
        public static final String KEY_IS_NEED_VERIFY = "is_need_verify";

        public MySharedPreferencesManager(Context context) {
            sp = context.getSharedPreferences(KEY_PASSWORD_DATA, Context.MODE_PRIVATE);
        }

        public static void savePassword(String password) {
            String MD5Password;
            try {
                MD5Password = password.length() == 0 ? "" : getMD5(password);
            } catch (Exception e) {
                MD5Password = "";
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(KEY_APP_PASSWORD, MD5Password);
            editor.apply();
        }

        public static boolean checkPassword(String password) {
            String MD5Password = sp.getString(KEY_APP_PASSWORD, "");
            try {
                assert MD5Password != null;
                return MD5Password.equals(getMD5(password));
            } catch (Exception e) {
                return false;
            }
        }

        public static boolean isNullPassword() {
            String password = sp.getString(KEY_APP_PASSWORD, "");
            assert password != null;
            return password.length() == 0;
        }

        public static boolean isNeedVerify() {
            return sp.getBoolean(KEY_IS_NEED_VERIFY, false);
        }


        public static void setNeedVerify(boolean isNeedVerify) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean(KEY_IS_NEED_VERIFY, isNeedVerify);
            editor.apply();
        }

        public static String getMD5(String val) throws NoSuchAlgorithmException {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(val.getBytes());
            byte[] m = md5.digest();//加密
            return getString(m);
        }

        private static String getString(byte[] bytes) {
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(aByte);
            }
            return sb.toString();
        }

    }


    public void setActionbarBackground(int resId) {
        actionbar.setBackgroundResource(resId);
    }

    public void setTextTitle(String title) {
        textTitle.setText(title);
    }

    public void setTextTitle(int resId) {
        textTitle.setText(resId);
    }

    public void setBackBtnVisibility(int visibility) {
        mBackBtn.setVisibility(visibility);
    }

    public void backBtnMethod(View view) {
        BasePasswordManagerActivity.this.finish();
    }

    /**
     * 初始化私有SharedPreferencesManager
     * 如果在启动页有启动activity
     * 则不需要单独再调用
     *
     * @param context context
     */
    public static void initMySharedPreferencesManager(Context context) {
        new MySharedPreferencesManager(context);
    }

    public static void startVerifyWhenReturn(Context context, Class c, PasswordManagerListener listener) {
        if (!isNeedVerify()) return;

        Intent in = new Intent(context, c);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.putExtra(KEY_ACTION, ACTION_LOGIN);
        context.startActivity(in);
        setPasswordManagerListener(listener);
    }

    public static void startSetPwdActivity(Context context, Class c, PasswordManagerListener listener) {
        Intent intent = new Intent(context, c);
        intent.putExtra(KEY_ACTION, ACTION_RESET_PASSWORD);
        setPasswordManagerListener(listener);
        context.startActivity(intent);
    }

    public static void switchClick(final Context context, final Class c, @NonNull final OperateCallback operateCallback) {
        MaterialDialog.ButtonCallback callback = new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                startSetPwdActivity(context, c, new PasswordManagerListener() {
                    @Override
                    public void authenticationComplete(Boolean isAuthenticationSuccess) {
                        if (isAuthenticationSuccess) {
                            setNeedVerify(true);
                            operateCallback.operateFinish(true);
                        }
                    }
                });
            }
        };

        if (isNullPassword()) {
            //无密码,先置回原样
            operateCallback.operateFinish(false);
            ZkDialogManager.createConfirmDialog(context, R.string.pwdNotSet, R.string.isJumpToSetPassword, callback).show();
        } else {
            setNeedVerify(!isNeedVerify());
            operateCallback.operateFinish(isNeedVerify());
        }
    }
}
