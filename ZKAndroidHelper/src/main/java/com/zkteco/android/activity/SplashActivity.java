package com.zkteco.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zkteco.android.R;
import com.zkteco.android.event.AdCallbackListener;
import com.zkteco.android.tool.ZKLog;
import com.zkteco.android.tool.ZKTool;

/**
 * The advertise activity.including mogoAds,flurry..
 *
 * @author allen.lan
 * @data 2015-07-14
 */

public class SplashActivity extends Activity {

    public static final int AD_PLATFORM_BAIDU = 0;

    public static final int AD_PLATFORM_ADMOB = 1;

    private static final String LOG_TAG = SplashActivity.class.getName();
    private Activity mAct;
    // ad platform need id to show ads.
    //private RelativeLayout mSplashAdLayoutView;
    private View view;
    // launcher layout
    private LinearLayout mSplashBgLayout;
    private RelativeLayout mProductShowLayout;
    private TextView mProductName;
    private TextView mProducrUrl;
    private ImageView mProductLogo;
    private Button mSkipBtn;

    private Handler mHandler = new Handler();
    private AdCallbackListener mAdCallbackListener;
    private Animation animation;

//	public Ad mAd;

    // whether play ad
    private boolean mIsPlayAd = false;
    // whether Distinguish region,chinese or others area
    private boolean mDistinguishRegion = false;
    // choose first ad platform,default baidu
    private int mInlandAdPlatform = AD_PLATFORM_BAIDU;
    // choose second ad platform,default admob
    private int mOverSeaAdPlatform = AD_PLATFORM_ADMOB;
    // first adID
    private String mInlandAdID;
    // second adID
    private String mOverSeaAdID;
    //wait load ad max time(ms)
    private int mWaitLoadAdMaxTime = 6000;

    public boolean ismIsPlayAd() {
        return mIsPlayAd;
    }

    public void setmIsPlayAd(boolean mIsPlayAd) {
        this.mIsPlayAd = mIsPlayAd;
    }

    public boolean ismDistinguishRegion() {
        return mDistinguishRegion;
    }

    public void setmDistinguishRegion(boolean mDistinguishRegion) {
        this.mDistinguishRegion = mDistinguishRegion;
    }

    public int getmInlandAdPlatform() {
        return mInlandAdPlatform;
    }

    public void setmInlandAdPlatform(int mInlandAdPlatform) {
        this.mInlandAdPlatform = mInlandAdPlatform;
    }

    public int getmOverSeaAdPlatform() {
        return mOverSeaAdPlatform;
    }

    public void setmOverSeaAdPlatform(int mOverSeaAdPlatform) {
        this.mOverSeaAdPlatform = mOverSeaAdPlatform;
    }

    public String getmInlandAdID() {
        return mInlandAdID;
    }

    public void setmInlandAdID(String mInlandAdID) {
        this.mInlandAdID = mInlandAdID;
    }

    public String getmOverSeaAdID() {
        return mOverSeaAdID;
    }

    public void setmOverSeaAdID(String mOverSeaAdID) {
        this.mOverSeaAdID = mOverSeaAdID;
    }

    public int getmWaitLoadAdMaxTime() {
        return mWaitLoadAdMaxTime;
    }

    public void setmWaitLoadAdMaxTime(int mWaitLoadAdMaxTime) {
        this.mWaitLoadAdMaxTime = mWaitLoadAdMaxTime;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        view = View.inflate(this, R.layout.activity_splash, null);
        setContentView(view);

        initView();
        animationView();
    }

    public void initView() {
        mSplashBgLayout = (LinearLayout) findViewById(R.id.splash_bg_layout);
        mProductShowLayout = (RelativeLayout) findViewById(R.id.product_show_layout);
        mProductLogo = (ImageView) findViewById(R.id.product_logo_img);
        mProductName = (TextView) findViewById(R.id.product_name_txt);
        mProducrUrl = (TextView) findViewById(R.id.product_url_txt);
        mSkipBtn = (Button) findViewById(R.id.skip_btn);

        mSkipBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                mAd.setmIsFinishLoadAd(true);
                mAdCallbackListener.onSplashClose();
            }
        });
    }

    public void setAnimationDuration(Long duration) {
        this.animation.setDuration(duration);
    }

    public void animationView() {
        animation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        view.startAnimation(animation);
        animation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startLoadAd();
            }
        });
    }

    /**
     * set Launcher bg pic
     *
     * @param resId
     */
    public void setLauncherBgPic(int resId) {
        if (mSplashBgLayout != null) {
            mSplashBgLayout.setBackgroundResource(resId);
        }
    }

    public void setProductLogoPic(int resId) {
        mProductLogo.setBackgroundResource(resId);
    }

    public void setProductName(String productName) {
        mProductName.setText(productName);
    }

    public void setProductUrl(String productUrl) {
        mProducrUrl.setText(productUrl);
    }

    public void setProductShowLayoutVisible(int visible) {
        mProductShowLayout.setVisibility(visible);
    }

    public void setSkipBtnVisible(int visible) {
        mSkipBtn.setVisibility(visible);
    }

    /**
     * @param pAct
     * @param pAdCallbackListener
     */
    public void initAd(Activity pAct, AdCallbackListener pAdCallbackListener) {
        this.mAct = pAct;
        this.mAdCallbackListener = pAdCallbackListener;
        ZKLog.d(LOG_TAG, "initAd");
        // if user is chinese,user MogoAd,others use google Admob
    }

    public void startLoadAd() {
        if (!mIsPlayAd) {
            mAdCallbackListener.onSplashClose();
            ;
        } else {
            if (mDistinguishRegion) {
                if (isChineseUser()) {
                    loadAd(mInlandAdID, mInlandAdPlatform);
                } else {
                    loadAd(mOverSeaAdID, mOverSeaAdPlatform);
                }
            } else {
                if (!ZKTool.checkStringNull(mInlandAdID)) {
                    loadAd(mInlandAdID, mInlandAdPlatform);
                } else if (!ZKTool.checkStringNull(mOverSeaAdID)) {
                    loadAd(mOverSeaAdID, mOverSeaAdPlatform);
                }
            }
        }

    }

    /**
     * load ad base on adid and adplatform
     *
     * @param pAdID
     * @param pAdPlatform
     */

    private void loadAd(String pAdID, int pAdPlatform) {
//		ZKLog.d(LOG_TAG, "loadad =" + pAdID + ",platform=" + pAdPlatform);
//		if (ZKTool.checkStringNull(pAdID)) {
//			Toast.makeText(mAct, R.string.ad_error, Toast.LENGTH_SHORT).show();
//			if (mAdCallbackListener != null) {
//				mAdCallbackListener.onSplashError("");
//			}
//			return;
//		}
//		ZKLog.d(LOG_TAG, "loadad go=");
//
//		switch (pAdPlatform) {
//		case AD_PLATFORM_BAIDU:
//			ZKLog.d(LOG_TAG, "loadBaiduSpalshAd=" + pAdID);
//			mAd = new BaiduSSPAd(this.mAct, pAdID,
//					view, this.mAdCallbackListener);
//			mAd.loadSplashAd();
//			break;
//		case AD_PLATFORM_ADMOB:
//
//			break;
//		default:
//			break;
//		}
//		mHandler.postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				ZKLog.d(LOG_TAG, "mAd.getmIsFinishLoadAd()" + mAd.getmIsFinishLoadAd());
//				if (mAd == null || (mAd != null && mAd.getmIsFinishLoadAd())) {
//					return;
//				}
//				mAd.setmIsFinishLoadAd(true);
//				mAdCallbackListener.onSplashClose();
//			}
//		}, mWaitLoadAdMaxTime);
    }

    /**
     * whether is chinese user,push different ad
     *
     * @return
     */
    private boolean isChineseUser() {
        return true;
    }


    protected void openActivity(Class<?> pClass) {
        openActivity(pClass, null);
    }

    protected void openActivity(Class<?> pClass, Bundle pBundle) {
        Intent intent = new Intent(this, pClass);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        startActivity(intent);
    }
}
