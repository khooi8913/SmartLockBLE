package com.zkteco.android.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.zkteco.android.tool.ZKLog;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by xqc on 2015/9/7.
 * ZKTeco app
 */
public class BaseApplication extends Application {
    private static final String TAG = BaseApplication.class.getName();
    private List<Activity> mList = new LinkedList<Activity>();
    private static boolean activityVisible = false;
    private static BaseApplication instance;

    public long timeOfAppStop = 0;
    public long timeOfAPPRestart;
    private int activityAlive;
    private int applicationBackstageTimeOut = 10000;

    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        activityAlive = 0;

        this.registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks);
    }

    public synchronized static BaseApplication getInstance() {
        if (null == instance) {
            instance = new BaseApplication();
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        mList.add(activity);
    }

    public void exit() {
        try {
            for (Activity activity : mList) {
                if (activity != null) {
                    activity.finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(0);
        }
    }

    //移除 除了指定的className之外的Activity
    public void exitWithoutActivity(String className) {
        try {
            for (Activity activity : mList) {
                if (activity != null && !activity.isFinishing() && !activity.getClass().getSimpleName().equals(className)) {
                    ZKLog.d(TAG, " clear Activity " + activity.getClass().getSimpleName());
                    activity.finish();
                }
            }
            ZKLog.d(TAG, " All Activities has been clear except " + className);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Context getContext() {
        return mContext;
    }

    ActivityLifecycleCallbacks mActivityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            getInstance().addActivity(activity);
            activityAlive++;
        }

        @Override
        public void onActivityStarted(Activity activity) {
            timeOfAPPRestart = System.currentTimeMillis();

            ZKLog.d(TAG," onActivity  Started executed -------------------");
//            ZKLog.d(TAG," onActivity  Started timeOfAPPRestart " + tempShow(timeOfAPPRestart)) ;
//            ZKLog.d(TAG," onActivity  Started timeOfAppStop " + tempShow(timeOfAppStop)) ;
//            ZKLog.d(TAG," onActivity  Started result "+((timeOfAppStop != 0 && timeOfAPPRestart - timeOfAppStop > applicationBackstageTimeOut)));

            if ( timeOfAppStop != 0 && timeOfAPPRestart - timeOfAppStop > applicationBackstageTimeOut) {
                onApplicationBackstageTimeOut(activity);
            }
            timeOfAppStop = 0;
        }

        @Override
        public void onActivityResumed(Activity activity) {
            activityResumed();
        }

        @Override
        public void onActivityPaused(Activity activity) {
            activityPaused();
        }

        @Override
        public void onActivityStopped(Activity activity) {
//            Log.d(TAG, "onActivityStopped");
            ZKLog.d(TAG, " onActivity  Stopped executed -------------------");
            if (!activityVisible) {
                timeOfAppStop = System.currentTimeMillis();
                executeWhenAppInBackground();
//                ZKLog.d(TAG," onActivity  Stopped timeOfAppStop " + tempShow(timeOfAppStop)) ;
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

        @Override
        public void onActivityDestroyed(Activity activity) {
            activityAlive--;
            if (activityAlive <= 0) {
                activityAlive = 0;
                timeOfAppStop = 0;
            }
        }
    };

//    public static boolean isActivityVisible() {
//        return activityVisible;
//    }

    /**
     * 设置应用后台超时时间,用于
     * 后台xx秒后返回要输入密码
     *
     * @param timeOut 后台超时的时间(单位/秒)
     */
    protected void setApplicationBackstageTimeOut(int timeOut) {
        applicationBackstageTimeOut = timeOut;
    }

    /**
     * 在应用后台运行一段时间后调用
     * 用于后台xx秒后返回要输入密码
     * 需要先调用setApplicationBackstageTimeOut()设置后台超时时间
     *
     * 请重写该函数
     */
    protected void onApplicationBackstageTimeOut(Activity activity) {

    }

    public void activityResumed() {
        activityVisible = true;

    }

    public void activityPaused() {
        activityVisible = false;
    }

//    private String tempShow(long yourmilliseconds){
//        if(yourmilliseconds==0L){
//            return "0";
//        }
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        Date resultdate = new Date(yourmilliseconds);
//        return sdf.format(resultdate);
//    }

    public void executeWhenAppInBackground(){}
}
