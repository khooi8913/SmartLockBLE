package com.zkteco.android.bluetooth;

import android.content.Context;
import android.util.Log;

import com.zkteco.android.bluetooth.ble.BluetoothManagerBLE;
import com.zkteco.android.bluetooth.spp.BluetoothManagerSPP;

public class ZKBluetoothFactory {

    public static final String TAG = ZKBluetoothFactory.class.getSimpleName();
    public static final int TYPE_SPP_BLUETOOTH = 20;
    public static final int TYPE_BLE_BLUETOOTH = 40;
    public static ZKBluetoothManager mZKBluetoothManager;

    public static ZKBluetoothManager generateBluetoothManager(Context context, int type) {
        Log.d(TAG, "my blue type is " + type);
        mZKBluetoothManager = null;
        switch (type) {
            case TYPE_SPP_BLUETOOTH:
                mZKBluetoothManager = new BluetoothManagerSPP(context);
                break;

            case TYPE_BLE_BLUETOOTH:
                mZKBluetoothManager = new BluetoothManagerBLE(context);
                break;

            default:
                break;
        }
        return mZKBluetoothManager;
    }

    public static ZKBluetoothManager getZKBluetoothManager() {
        return mZKBluetoothManager;
    }

    public static BluetoothManagerBLE getBluetoothManagerBLE() {
        return (BluetoothManagerBLE) mZKBluetoothManager;
    }

    public static BluetoothManagerSPP getBluetoothManagerSPP() {
        return (BluetoothManagerSPP) mZKBluetoothManager;
    }
}
