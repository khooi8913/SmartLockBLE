package com.zkteco.android.bluetooth;

import android.content.Context;

import com.zkteco.android.tool.ZKLog;

public class ZKBluetoothManager {
    public static final String TAG = ZKBluetoothManager.class.getSimpleName();

    private Context mContext;

    public ZKBluetoothManager(Context context) {
        mContext = context;
    }

    public int read(byte[] buffer, int offset, int count) {
        return 0;
    }

    public int write(byte[] buffer, int offset, int count) {
        return 0;
    }

    public void cleanPullDataBuffer() {
        ZKLog.e(TAG, "null function,please overwrite it");
    }

    public String getBluetoothAddress() {
        return "";
    }
}
