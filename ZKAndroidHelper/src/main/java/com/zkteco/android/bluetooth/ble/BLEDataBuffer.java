/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
           佛祖保佑       永无BUG
 */
package com.zkteco.android.bluetooth.ble;


import java.util.ArrayList;
import java.util.List;

public class BLEDataBuffer {
    private static final String TAG = BLEDataBuffer.class.getSimpleName();

    private final static int BLE_DATA_UNIT_LENGTH = 20;
    private final static int BYTE_ARRAY_LENGTH = 512;

    private List<byte[]> mDataList;
    private int mLength;
    //    private int mAvailable;
    private int lastReadOffset;
    //    private int mPosition;
    private int mUnitCount;

    public BLEDataBuffer() {
        mDataList = new ArrayList<>();
        byte[] bytes = new byte[BYTE_ARRAY_LENGTH];
        mDataList.add(bytes);
        mUnitCount = 0;
    }

    public synchronized void append(byte[] data) {
        for (int i = 0; i < data.length; i++) {
//            mUnitCount=mDataList.size();
//            mLength++;
            if (mLength >= mDataList.size() * BYTE_ARRAY_LENGTH) {
                byte[] bytes = new byte[BYTE_ARRAY_LENGTH];
                mDataList.add(bytes);
                mUnitCount++;
            }
            mDataList.get(mUnitCount)[(mLength % BYTE_ARRAY_LENGTH)] = data[i];
            mLength++;
        }
//        for (byte[] bytes : mDataList) {
//            Log.d(TAG, ZKTool.getHexString(bytes, true));
//        }
//        for (byte[] bytes : mDataList) {
//            Log.d(TAG, new String(bytes));
//        }
    }

    public synchronized int read(byte[] buffer, int offset, int count) {
        int currentUnit;
        int i;
        for (i = 0; i < count; i++) {
            currentUnit = (lastReadOffset + i) / BYTE_ARRAY_LENGTH;
            buffer[i] = mDataList.get(currentUnit)[(lastReadOffset + i) % BYTE_ARRAY_LENGTH];
        }
        lastReadOffset += count;
//        Log.d(TAG, "get data +" + ZKTool.getHexString(buffer, offset, count, true));
//        Log.d(TAG, "get string ");
//        Log.d(TAG, new String(buffer));
        return i;
    }

    public synchronized int available() {
        return mLength - lastReadOffset;
    }

    public synchronized void clean() {
        mDataList.clear();
        mLength = 0;
        lastReadOffset = 0;
        mUnitCount = 0;
    }
}
