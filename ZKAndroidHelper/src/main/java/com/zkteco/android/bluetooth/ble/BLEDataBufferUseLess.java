package com.zkteco.android.bluetooth.ble;


import java.util.ArrayList;
import java.util.List;

public class BLEDataBufferUseLess {

    private final static int BLE_DATA_UNIT_LENGTH = 20;
    private final static int BYTE_ARRAY_LENGTH = 32;

    private List<byte[]> mDataList;
    private int mLength;
    //    private int mPosition;
    private int mUnitCount;

    public BLEDataBufferUseLess() {
        mDataList = new ArrayList<>();
        byte[] bytes = new byte[BYTE_ARRAY_LENGTH];
        mDataList.add(bytes);
        mUnitCount = mDataList.size();
    }

    public void append(byte[] data) {
        int dataLength = data.length;         //需写入的总长度
        int dataOffset = 0;                   //当前开始写入的位置
        int copyLength = 0;
        while (dataOffset < dataLength) {
            //计算出可写入长度
            copyLength = BYTE_ARRAY_LENGTH - mLength % BYTE_ARRAY_LENGTH;
            //如果可写入长度为0，则添加一个数组
            if (copyLength == 0) {
                byte[] bytes = new byte[BYTE_ARRAY_LENGTH];
                mDataList.add(bytes);
                mUnitCount = mDataList.size();
                continue;
            }
            //获取需写入数据
            if (dataLength - dataOffset < copyLength) {
                copyLength = dataLength - dataOffset;
            }

            System.arraycopy(data, dataOffset,
                    mDataList.get(mUnitCount - 1), mLength % BYTE_ARRAY_LENGTH,
                    copyLength);
            //复制完，标识位更新
            dataOffset += copyLength;
            mLength += copyLength;
        }
    }

    public void read(byte[] buffer, int offset, int count) {
        int copyLength = 0;
        boolean isJump2Next = false;

        int copyUnitPosition = offset / BYTE_ARRAY_LENGTH;
        int dataUnitOffset = 0;

        int bufferOffset = 0;
        int haveReadOffset = offset;      //用于记录已读数据

        while (haveReadOffset < offset + count) {

            copyLength = BYTE_ARRAY_LENGTH - haveReadOffset % BYTE_ARRAY_LENGTH;
//            if (dataUnitOffset == 0) {
//                copyUnitPosition++;
//                continue;
//            }
            //获取需要读到的数据长度,如果长度大于剩余长度，则标记读取完成后，跳至下一个数组
            if (offset + count - haveReadOffset < copyLength) {
                copyLength = offset + count - haveReadOffset;
            } else isJump2Next = true;

            System.arraycopy(mDataList.get(copyUnitPosition), offset % BYTE_ARRAY_LENGTH,
                    buffer, bufferOffset, copyLength);

            bufferOffset += copyLength;
            haveReadOffset += copyLength;
            if (isJump2Next) {
                copyUnitPosition++;
                isJump2Next = false;
            }
        }
    }

    public void clean() {
        mDataList.clear();
    }


    //    public void write() {
//
//    }
//    private class BleUnitData {
//        private byte[] data;
//
//    }

}
