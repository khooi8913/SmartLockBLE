/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zkteco.android.bluetooth.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Build;

import com.zkteco.android.tool.ZKLog;
import com.zkteco.android.tool.ZKTool;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BluetoothLeService {
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    public final static int ACTION_GATT_CONNECTED = 10;
    public final static int ACTION_GATT_DISCONNECTED = 11;
    public final static int ACTION_GATT_SERVICES_DISCOVERED = 12;
    public final static int ACTION_DATA_AVAILABLE = 13;
    public final static int EXTRA_DATA = 14;

    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    private boolean isBusy;
    //需要同时添加一个read的队列
    //无回应的情况下,可能丢失后续部分
    //优先级  descriptorWriteQueue > characteristicWriteQueue
    private Queue<BluetoothGattDescriptor> descriptorWriteQueue = new LinkedList<BluetoothGattDescriptor>();
    private Queue<BluetoothGattCharacteristic> characteristicWriteQueue = new LinkedList<BluetoothGattCharacteristic>();

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public synchronized void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            if (ZKTool.DEBUG) {
                ZKLog.d(TAG, "Write Characteristic state is: " + status + " Uuid is :" + characteristic.getUuid().toString());
                ZKLog.e(TAG, "----write callback--------" + ZKTool.getHexString(characteristic.getValue(), true));
            }
            characteristicWriteQueue.remove();

            if (characteristicWriteQueue.size() > 0) {
                mBluetoothGatt.writeCharacteristic(characteristicWriteQueue.element());
            } else {
                isBusy = false;
            }

            notifyAll();
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            if (ZKTool.DEBUG) {
                ZKLog.d(TAG, "Write Descriptor state is: " + status + " Uuid is :" + descriptor.getCharacteristic().getUuid().toString());
            }
            descriptorWriteQueue.remove();
            if (descriptorWriteQueue.size() > 0) {
                mBluetoothGatt.writeDescriptor(descriptorWriteQueue.element());
            } else if (characteristicWriteQueue.size() > 0) {
                mBluetoothGatt.writeCharacteristic(characteristicWriteQueue.element());
            }
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            ZKLog.d(TAG, "onConnectionStateChange");
            int intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                ZKLog.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                ZKLog.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                ZKLog.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            ZKLog.e(TAG, "onServicesDiscovered");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                ZKLog.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            ZKLog.e(TAG, "onCharacteristicRead");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public synchronized void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if (ZKTool.DEBUG) {
                ZKLog.e(TAG, "onCharacteristicChanged-----------" +
                        ZKTool.getHexString(characteristic.getValue(), true));
            }
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }

    };

    private void broadcastUpdate(final int action) {
        if (mBluetoothLeCallback == null) return;


        mBluetoothLeCallback.onConnectionStateChange(action);

        if (action == ACTION_GATT_SERVICES_DISCOVERED) {
            mBluetoothLeCallback.onServicesDiscovered();
        }

    }

    private void broadcastUpdate(final int action,
                                 final BluetoothGattCharacteristic characteristic) {
        if (mBluetoothLeCallback != null) {
            mBluetoothLeCallback.onCharacteristicChanged(action, characteristic);
        }
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize(Context context) {
//         For API level 18 and above, get a reference to BluetoothAdapter through

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                ZKLog.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            ZKLog.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(Context context, final String address) {
        if (mBluetoothAdapter == null || address == null) {
            ZKLog.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
//        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
//                && mBluetoothGatt != null) {
//            ZKLog.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
//            if (mBluetoothGatt.connect()) {
//                mConnectionState = STATE_CONNECTING;
//                return true;
//            } else {
//                return false;
//            }
//        }
        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            ZKLog.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(context, false, mGattCallback);
        ZKLog.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            ZKLog.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            ZKLog.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Request a write on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicChanged(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     * You also need to set CharacteristicNotification fist,so you can get the answer for this.
     *
     * @param characteristic The characteristic to write to.
     */
    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            ZKLog.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        if (ZKTool.DEBUG) {
//            ZKLog.d(TAG, "write Characteristic value is " + ZKTool.getHexString(characteristic.getValue(), true));
            ZKLog.d(TAG, "writing Characteristic...");
        }

        characteristicWriteQueue.add(characteristic);

        if (characteristicWriteQueue.size() == 1 && descriptorWriteQueue.size() == 0) {
            mBluetoothGatt.writeCharacteristic(characteristic);
            isBusy = true;
        }
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            ZKLog.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        //写入characteristic的配置Descriptor
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);//ENABLE_INDICATION_VALUE
//        ZKLog.d(TAG, "show time writeDescriptor");
//        mBluetoothGatt.writeDescriptor(descriptor);
        writeGattDescriptor(descriptor);
    }

    //添加至队列,防止冲突
    public void writeGattDescriptor(BluetoothGattDescriptor descriptor) {
        descriptorWriteQueue.add(descriptor);
        if (descriptorWriteQueue.size() == 1) {
            mBluetoothGatt.writeDescriptor(descriptor);
        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setBluetoothMTU(int mtu) {
        boolean flag = mBluetoothGatt.requestMtu(mtu);
        ZKLog.e(TAG, "setting MTU~~~~~~~~~~~~~~~~~~~~~" + flag);
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    public int getConnectionState() {
        return mConnectionState;
    }

    private static BluetoothLeCallback mBluetoothLeCallback;

    public void setBluetoothLeCallback(BluetoothLeCallback bluetoothLeCallback) {
        mBluetoothLeCallback = bluetoothLeCallback;
    }

    public interface BluetoothLeCallback {

        void onConnectionStateChange(int intentAction);

        void onServicesDiscovered();

        void onCharacteristicChanged(int intentAction, BluetoothGattCharacteristic characteristic);
    }
}
