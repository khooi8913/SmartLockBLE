package com.zkteco.android.bluetooth.ble;


import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.util.Log;

import com.zkteco.android.bluetooth.ZKBluetoothManager;
import com.zkteco.android.bluetooth.device.ZKBluetoothDevice;
import com.zkteco.android.tool.ZKLog;
import com.zkteco.android.tool.ZKTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BluetoothManagerBLE extends ZKBluetoothManager {
    private static final String TAG = BluetoothManagerBLE.class.getSimpleName();
    private static final int ERROR_CODE_PULL_CONNECT_LOST = -2;
    private static final int SCAN_PERIOD = 5000;
    private static final int DEFAULT_WAIT_TIME = 5000;
    private static final int DEFAULT_TIME_INTERVAL = 50;

    private boolean isGattSearchDown = false;
    private boolean isConnecting = false;
    //    private static BluetoothGattService mBluetoothGattService;
    private static HashMap<String, BluetoothGattCharacteristic> mGattCharacteristics = new HashMap<>();

    private static BluetoothLeService mBluetoothLeService;
    private static BLEDataBuffer mBleDataBuffer;

    private Context mContext;
    private Handler mHandler;
    private boolean mScanning;
    private boolean isHandleCallback = true;

    private static ZKBluetoothDevice mBluetoothDevice;
    private static BluetoothAdapter mBluetoothAdapter;

    private UUID[] mServiceUuids;

    public BluetoothManagerBLE(Context context) {
        super(context);
        this.mContext = context;
        mHandler = new Handler();
        initialize();
    }

    //检查连接状态
    //检查刷新状态

    private void initialize() {
        mBluetoothLeService = new BluetoothLeService();
        mBluetoothLeService.initialize(mContext);
        mBluetoothLeService.setBluetoothLeCallback(mBluetoothLeCallback);
        mBluetoothAdapter = mBluetoothLeService.getBluetoothAdapter();
    }

    /**
     * 获取蓝牙网卡地址
     *
     * @return 蓝牙网卡地址
     */
    @Override
    public String getBluetoothAddress() {
        return mBluetoothAdapter.getAddress();
    }

    /**
     * 打开蓝牙
     * (高版本系统不提示确认)
     */
    public void enableBluetooth() {
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
    }

    public void enableBluetoothSystemDialog() {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            enableBtIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(enableBtIntent);
        }
    }

    public void disableBluetooth() {
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }
    }

    public boolean isEnableBluetooth() {
        return ((mBluetoothAdapter != null) && (mBluetoothAdapter.isEnabled()));
    }

    @Deprecated
    public void setBluetoothScanParameters(
            UUID[] serviceUuids, BluetoothAdapter.LeScanCallback bluetoothLeCallback) {
        mServiceUuids = serviceUuids;
        setLeScanCallback(bluetoothLeCallback);
    }


    /**
     * 设置搜索回调
     * 需要处理返回空设备情况(代表搜索结束)
     *
     * @param leScanCallback 回调函数
     */
    @Deprecated
    public void setLeScanCallback(BluetoothAdapter.LeScanCallback leScanCallback) {
        mLeScanCallback = leScanCallback;
    }

    /**
     * 清空管理器状态
     */
    public void clean() {
//        disableNotify();
        mGattCharacteristics.clear();

        mBluetoothLeService.close();
        mBluetoothLeService = null;
        initialize();
    }

    /**
     * 设置MTU 需要在安卓5.0以上系统才能支持
     * 同时也需要硬件支持
     * (默认23,因蓝牙包头占用3byte,所以蓝牙4.0一次只能发送20byte数据)
     *
     * @param mtu 蓝牙发送的最大包长
     */
    public void setBluetoothMTU(int mtu) {
        ZKLog.d(TAG, "trying to set MTU");
        mBluetoothLeService.setBluetoothMTU(mtu);
    }


    /**
     * 断开连接
     */
    public void disconnect() {
        disconnect(false);
    }

    public void disconnect(boolean isHandleCallback) {
        if (mBluetoothLeService != null) {
            try {
                this.isHandleCallback = isHandleCallback;
                mBluetoothLeService.disconnect();
            } catch (Exception e) {
                ZKLog.e(TAG, "disconnect: error", e);
            }
        }
    }

    /**
     * 获取当前连接状态
     * BluetoothLeService.STATE_DISCONNECTED = 0;
     * BluetoothLeService.STATE_CONNECTING = 1;
     * BluetoothLeService.STATE_CONNECTED = 2;
     *
     * @return 当前连接状态
     */
    public int getConnectState() {
        return mBluetoothLeService.getConnectionState();
    }

    /**
     * 获取当前连接的设备
     * 在函数中会先检查当前的连接状态再返回
     *
     * @return 如果已连接, 则返回当前设备
     * 否者返回null
     */
    public ZKBluetoothDevice getBluetoothDevice() {
        return checkConnectState() ? mBluetoothDevice : null;
    }

    /**
     * 检查当前连接状态
     *
     * @return 如果当前已连接设备, 则返回 true
     */
    public boolean checkConnectState() {
        return getConnectState() == BluetoothLeService.STATE_CONNECTED;
    }

    /**
     * 清空pull协议的缓存,防止因为某次意外的数据错误
     * 照成之后所有的通信发生数据错误等问题
     */
    @Override
    public void cleanPullDataBuffer() {
        mBleDataBuffer = new BLEDataBuffer();
    }

    /**
     * 用于pull的调用,读取pull缓存中的数据
     *
     * @param buffer 用于装载缓存数据
     * @param offset 读取的当前数据的启始位置,每次新的pull通信都会从0开始
     * @param count  读取的数据量
     * @return 实际读取的长度
     */
    public int read(byte[] buffer, int offset, int count) {
        try {
            int len;
            int readable = mBleDataBuffer.available();
            if (readable > 0) {
                ZKLog.d(TAG, "pull read offset = " + offset);
                ZKLog.d(TAG, "pull read count = " + count);
                len = readable > count ? count : readable;
                len = mBleDataBuffer.read(buffer, offset, len);
                if (ZKTool.DEBUG) {
                    ZKLog.d(TAG, "--------pull read ------" + ZKTool.getHexString(buffer, offset, len, true));
                }
                return len;
            }
        } catch (Exception e) {
            ZKLog.e(TAG, "read error", e);
            return 0;
        }
        return checkConnectState() ? 0 : ERROR_CODE_PULL_CONNECT_LOST;
    }

    /**
     * pull的写入函数,需要先设置pull的写入 特征
     * 拆包操作在pull处完成,默认都是已经拆分成20byte的数据
     * 线程会等待实际写入成功的回调
     *
     * @param buffer 用于装载缓存数据
     * @param offset 写入的当前数据的启始位置,每次新的pull通信都会从0开始
     * @param count  写入的数据量
     * @return 等于调用时的写入长度(并不是实际写入长度)
     */
    public synchronized int write(byte[] buffer, int offset, int count) {
        BluetoothGattCharacteristic gattCharacteristic =
                mGattCharacteristics.get(BioBTGattAttributes.CHARACTERISTIC_PULL);

        if (gattCharacteristic == null) return 0;

        //是否需要截取buffer中的内容
        if (ZKTool.DEBUG) {
            ZKLog.d(TAG, "-----pull write-----" + ZKTool.getHexString(buffer, offset, count, true));
        }
        gattCharacteristic.setValue(buffer);
        mBluetoothLeService.writeCharacteristic(gattCharacteristic);

        try {
            ZKLog.d(TAG, "start wait");
            wait(20);
            ZKLog.d(TAG, "end wait");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return checkConnectState() ? count : ERROR_CODE_PULL_CONNECT_LOST;
    }

    /**
     * 直接发送指令到蓝牙模块,一般用于AT指令的发送
     * 会自动拆分成20byte的小数据包进行发送
     *
     * @param CMDString 要发送的操作指令
     */
    public void sendCMDString(String CMDString) {
        byte[] CMDByte = CMDString.getBytes();
        ZKLog.d(TAG, CMDString);
        sendCMDByte(CMDByte);
    }

    public void sendCMDByte(byte[] CMDByte) {
        BluetoothGattCharacteristic gattCharacteristic =
                mGattCharacteristics.get(BioBTGattAttributes.CHARACTERISTIC_CMD);
        if (gattCharacteristic == null) return;

        int Length = CMDByte.length;
        int offset = 0;
        int copyLength;
        while (offset < Length) {
            copyLength = Length - offset > 20 ? 20 : Length - offset;
            byte[] buffer = new byte[copyLength];
            System.arraycopy(CMDByte, offset, buffer, 0, copyLength);
            gattCharacteristic.setValue(buffer);
            mBluetoothLeService.writeCharacteristic(gattCharacteristic);
            //暂时替代
            ZKTool.threadSleep(10);

            offset += copyLength;
        }
    }


    /**
     * 对BluetoothLeCallback的封装
     * 将单个回调拆分成多个回调,方便调用
     * <p/>
     * 在该函数中还处理pull数据的缓存
     * 直接发送指令的结果返回,需要设置回调
     */
    private BluetoothLeService.BluetoothLeCallback mBluetoothLeCallback =
            new BluetoothLeService.BluetoothLeCallback() {

                @Override
                public void onConnectionStateChange(int intentAction) {
//                    if (mZKBleConnectStateListener != null) {
//                        mZKBleConnectStateListener.onConnectionStateChange(intentAction);
//                    }
                    //蓝牙断开的自动回调函数
                    if (intentAction == BluetoothLeService.ACTION_GATT_DISCONNECTED) {
                        mBluetoothDevice = null;
//                        disableNotify();
                        clean();
                        ZKLog.e(TAG, " bluetooth has been offline ! isConnecting : " + isConnecting);
                        //这里仅处理设备在连接成功之后掉线的情况,设备首次连接失败不给予处理.
                        if (isHandleCallback && !isConnecting) {
                            ZKLog.d(TAG, "mZKBleConnectStateChangeListenerList size " + mConnectionLostListenerList.size());
                            for (ConnectionLostListener connectionLostListener : mConnectionLostListenerList) {
                                connectionLostListener.onConnectLost();
                            }
                        }
                        isHandleCallback = true;
                    }
                }

                @Override
                public void onServicesDiscovered() {
                    ZKLog.d(TAG, "onServicesDiscovered");
                    filterAndSaveGattService(mBluetoothLeService.getSupportedGattServices());
                    //搜索服务完成，才算连接成功
                    isGattSearchDown = true;
                }

                @Override
                public void onCharacteristicChanged(int intentAction, BluetoothGattCharacteristic characteristic) {
                    if (characteristic.getUuid().toString().equals(BioBTGattAttributes.CHARACTERISTIC_CMD)) {
                        if (mZKBleCharacteristicListener != null) {
                            mZKBleCharacteristicListener.onCharacteristicChanged(intentAction, characteristic);
                        }
                        return;
                    }
                    //其他情况
                    mBleDataBuffer.append(characteristic.getValue());
                }
            };

    /**
     * 将连接之后,搜索出来的server进行遍历
     * 找到有用的service,将其保存
     * 然后在对其下的 特性 进行保存和 使能监听
     *
     * @param gattServices 该蓝牙设备下的所有servers
     */
    private void filterAndSaveGattService(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        for (BluetoothGattService gattService : gattServices) {
            if (gattService.getUuid().toString().equals(BioBTGattAttributes.SERVICE_BIO_BT)) {
//                mBluetoothGattService = gattService;
                saveAndNotifyCharacteristics(gattService.getCharacteristics());
                break;
            }
        }
    }

    /**
     * 将列表中的特征进行保存,然后使能他们的 监听
     * (可以同时监听多个特征,但是该操作需要放入队列中,不能并发使能,要等待回调)
     * (已在底层处理)
     *
     * @param characteristics 需要保存和使能的 特征
     */
    private void saveAndNotifyCharacteristics(List<BluetoothGattCharacteristic> characteristics) {
        mGattCharacteristics.clear();
        for (BluetoothGattCharacteristic gattCharacteristic : characteristics) {
            mGattCharacteristics.put(gattCharacteristic.getUuid().toString(), gattCharacteristic);
//            if (BioBTGattAttributes.CHARACTERISTIC_CMD.equals(gattCharacteristic.getUuid().toString())) {
            mBluetoothLeService.setCharacteristicNotification(gattCharacteristic, true);
        }
    }

    /**
     * 关闭所有的特征监听
     */
    private void disableNotify() {
        for (Object o : mGattCharacteristics.entrySet()) {
            Map.Entry entry = (Map.Entry) o;
            BluetoothGattCharacteristic gattCharacteristic = (BluetoothGattCharacteristic) entry.getValue();
            mBluetoothLeService.setCharacteristicNotification(gattCharacteristic, false);
        }
    }

    /**
     * 蓝牙断开的回调处理监听器的集合
     */
    private static List<ConnectionLostListener> mConnectionLostListenerList = new ArrayList<ConnectionLostListener>();

    public void addConnectionLostListener(ConnectionLostListener connectionLostListener) {
        if (!mConnectionLostListenerList.contains(connectionLostListener)) {
            mConnectionLostListenerList.add(connectionLostListener);
        }
    }

    public static List<ConnectionLostListener> getConnectionLostListenerList() {
        return mConnectionLostListenerList;
    }

    public interface ConnectionLostListener {
        void onConnectLost();
    }

    /**
     * 直接发送指令的回调
     */
    private static ZKBleCharacteristicListener mZKBleCharacteristicListener;

    /**
     * 直接发送指令的回调
     * 配合sendCMDString使用
     *
     * @param zkBleCharacteristicListener 接收到数据的回调
     */
    public void setZKBleCharacteristicListener(ZKBleCharacteristicListener zkBleCharacteristicListener) {
        mZKBleCharacteristicListener = zkBleCharacteristicListener;
    }

    /**
     * 直接发送指令的接收回调
     */
    public interface ZKBleCharacteristicListener {
        void onCharacteristicChanged(int intentAction, BluetoothGattCharacteristic characteristic);
    }


    public boolean connectBlock(final ZKBluetoothDevice device) {
        if (mBluetoothLeService == null) initialize();
        if (device == null) return false;

        mBleDataBuffer = new BLEDataBuffer();
        mBluetoothDevice = device;
        isGattSearchDown = false;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mBluetoothLeService.connect(mContext, device.getAddress());
            }
        });
        int timeOut = DEFAULT_WAIT_TIME;
        int waiteTime = 0;
        while (waiteTime < timeOut) {
            if (checkConnectState() && isGattSearchDown) break;
            ZKTool.threadSleep(DEFAULT_TIME_INTERVAL);
            waiteTime += DEFAULT_TIME_INTERVAL;
        }
        if (checkConnectState()) {
            return true;
        } else {
            clean();
            return false;
        }
    }

    public void setIsConnecting(boolean isConnecting) {
        this.isConnecting = isConnecting;
    }


    //**************************************//
    public interface ConnScanCallback {
        void deviceFind(ZKBluetoothDevice device);

        void scanStop();
    }

    public void scanLe(final boolean enable, String uuid, final ConnScanCallback connScanCallback) {
        if (connScanCallback != null) {
            mConnScanCallback = connScanCallback;
        } else return;
        if (isCannotFilter()) uuid = null;   //对三星S6进行特殊处理
        mScanning = enable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lollipopScan(enable, uuid);
        } else {
            kitkatScan(enable, uuid);
        }
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scanLe(false, null, connScanCallback);
                    if (mConnScanCallback != null) mConnScanCallback.scanStop();
                }
            }, SCAN_PERIOD);
        } else {
            if (mConnScanCallback != null) mConnScanCallback.scanStop();
        }
    }

    public void stopScan() {
        scanLe(false, null, mConnScanCallback);
    }

    private ConnScanCallback mConnScanCallback;
    //不能直接new，，否则4.+会崩溃
    private ScanCallback mScanCallback;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private ScanCallback generateScanCallback() {
        if (mScanCallback != null) return mScanCallback;
        mScanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                if (mConnScanCallback == null) return;
                mConnScanCallback.deviceFind(new ZKBluetoothDevice(result));
            }
        };
        return mScanCallback;
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (mConnScanCallback == null) return;
            mConnScanCallback.deviceFind(
                    new ZKBluetoothDevice(device, rssi, scanRecord, System.currentTimeMillis()));

        }
    };

    private void kitkatScan(boolean enable, String uuid) {
        ZKLog.d(TAG, "kitkatScan: ------------" + enable);
        if (uuid != null) mServiceUuids = new UUID[]{UUID.fromString(uuid)};

        if (enable) {
            if (mServiceUuids == null) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else mBluetoothAdapter.startLeScan(mServiceUuids, mLeScanCallback);
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void lollipopScan(boolean enable, String uuid) {
        ZKLog.d(TAG, "lollipopScan: ------------" + enable);
        BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();
        if (scanner == null) return;
        if (!enable) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(generateScanCallback());
//            if (scanner != null) {
//                scanner.stopScan(generateScanCallback());
//            } else {
//                mBluetoothAdapter.getBluetoothLeScanner().stopScan(generateScanCallback());
//            }
            return;
        }

        List<ScanFilter> filters = new ArrayList<>();
        if (uuid != null) {
            ScanFilter filter = new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(uuid)).build();
            filters.add(filter);
        }
//        scanner.startScan(filters, new ScanSettings.Builder().build(), mScanCallback);
        scanner.startScan(filters, new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build(), generateScanCallback()); //9次
//        scanner.startScan(filters, getScanSetting(), generateScanCallback());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private ScanSettings getScanSetting() {
        int scanMode;
        Log.d(TAG, "getScanMode: " + Build.BRAND);
        if (Build.BRAND.equals("HUAWEI")) {  //字段未确定
            scanMode = ScanSettings.SCAN_MODE_LOW_POWER;
        } else {
            scanMode = ScanSettings.SCAN_MODE_LOW_LATENCY;
        }
        return new ScanSettings.Builder().setScanMode(scanMode).build();
    }

    public boolean isCannotFilter() {
        return "SM-G9200".equals(Build.MODEL);
    }

    public BluetoothDevice generateBtDevice(String address) {
        if (mBluetoothAdapter != null) {
            return mBluetoothAdapter.getRemoteDevice(address);
        } else return null;
    }
}
