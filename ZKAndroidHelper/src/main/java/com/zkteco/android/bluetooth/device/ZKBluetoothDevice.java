package com.zkteco.android.bluetooth.device;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanResult;
import android.os.Build;
import android.text.TextUtils;

import com.zkteco.android.bluetooth.ZKBluetoothFactory;
import com.zkteco.android.tool.ZKLog;
import com.zkteco.android.tool.ZKTool;

/**
 * Created by xqc on 2015/9/10.
 * ZKTeco app
 */
public class ZKBluetoothDevice {
    public static final String TAG = "ZKBluetoothDevice";
    public static final int CODE_ZK = 0x5A4B;
    public static final int DATA_TYPE_MAC = 0x02;
    public static final int DATA_TYPE_DEVICE_TYPE = 0x03;
    public static final int NAMEFLAG_DEFAULT = 0;
    public static final int NAMEFLAG_SCANRECORD = 1;

    private BluetoothDevice mDevice;
    private int mRssi;
    private byte[] mByteScanRecord;
    private ZkScanRecord mScanRecord;
    private final long mTimestamp;
    private ZkBtDeviceType mDeviceType;
    private int mDeviceTypeCode;
    // 记录扫描结果内的设备名
    private String mRecordDeviceName = null;
    // 设备名称Flag(判断设备显示名称是以BluetoothDevice.getName()为准，还是ScanRecord.getDeviceName()为准)
    //  0 为默认是BluetoothDevice.getName()为准 1为ScanRecord.getDeviceName()为准
    private int mDeviceNameFlag = NAMEFLAG_DEFAULT;
    /**
     * Instantiates a new Bluetooth LE device.
     *
     * @param device         a standard android Bluetooth device
     * @param rssi           the RSSI value of the Bluetooth device
     * @param byteScanRecord the scan record of the device
     * @param timestamp      the timestamp of the RSSI reading
     */
    public ZKBluetoothDevice(BluetoothDevice device, int rssi, byte[] byteScanRecord, long timestamp) {
        mDevice = device;
        mRssi = rssi;
        mByteScanRecord = byteScanRecord;
        mScanRecord = ZkScanRecord.parseFromBytes(mByteScanRecord);
        decodeManufacturerData(mScanRecord);
        mTimestamp = timestamp;
    }

    public ZKBluetoothDevice(BluetoothDevice device, ZkBtDeviceType deviceType, int deviceTypeCode) {
        mDevice = device;
        mDeviceType = deviceType;
        mDeviceTypeCode = deviceTypeCode;
        mTimestamp = 0;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ZKBluetoothDevice(ScanResult result) {
        mDevice = result.getDevice();
        mRssi = result.getRssi();
        if (result.getScanRecord() != null) {
            // 某些手机 BluetoothDevice获取到的名称不是最新的名称，需要设置扫描结果获取的名称(比如魅族手机)
            String scanDeviceName = result.getScanRecord().getDeviceName();
            if(!TextUtils.isEmpty(scanDeviceName)){
                mRecordDeviceName = scanDeviceName;
            }
            decodeManufacturerData(result.getScanRecord().getManufacturerSpecificData(CODE_ZK));
        }
        mTimestamp = result.getTimestampNanos();
    }

    private void decodeManufacturerData(ZkScanRecord scanRecord) {
        decodeManufacturerData(scanRecord.getManufacturerSpecificData(CODE_ZK));
    }

    private void decodeManufacturerData(byte[] data) {
        if ((null == data) || (data.length == 0)) {
            mDeviceType = ZkBtDeviceType.BIO_LOCK;
            mDeviceTypeCode = mDeviceType.getDeviceTypeCode();
            return;
        }
        int currentPos = 0;

        try {
            while (currentPos < data.length) {
                int length = data[currentPos++] & 0xFF;
                if (length == 0) {
                    break;
                }
                // Note the length includes the length of the field type itself.
                int dataLength = length - 1;
                int fieldType = data[currentPos++] & 0xFF;
                switch (fieldType) {
                    case DATA_TYPE_MAC:
                        //this data is used for IOS
                        //in android we can get the mac address direct
                        break;
                    case DATA_TYPE_DEVICE_TYPE:
                        //高位在右
                        mDeviceTypeCode =
                                ((data[currentPos + 1] & 0xFF) << 8) + (data[currentPos] & 0xFF);
                        mDeviceType = ZkBtDeviceType.getDeviceTypeByCode(mDeviceTypeCode);
                        break;
                }
                currentPos += dataLength;
            }
        } catch (Exception e) {
            ZKLog.e(TAG, "decode scanRecord error", e);
        }
    }

    private void parseDeviceType(byte[] data, int currentPos, int dataLength) {


    }

    public BluetoothDevice getDevice() {
        return mDevice;
    }

    public int getRssi() {
        return mRssi;
    }

    public ZkScanRecord getScanRecord() {
        return mScanRecord;
    }

    public byte[] getByteScanRecord() {
        return mByteScanRecord;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public String getAddress() {
        return mDevice.getAddress();
    }

    public ZkBtDeviceType getDeviceType() {
        return mDeviceType;
    }

    public int getDeviceTypeCode() {
        return mDeviceTypeCode;
    }

    public String getName() {
        if(!TextUtils.isEmpty(mRecordDeviceName) && mDeviceNameFlag == NAMEFLAG_SCANRECORD){
            return mRecordDeviceName;
        }
        return mDevice.getName();
    }

    public void setDeviceType(ZkBtDeviceType deviceType) {
        mDeviceType = deviceType;
    }

    public int getmDeviceNameFlag() {
        return mDeviceNameFlag;
    }

    public void setmDeviceNameFlag(int mDeviceNameFlag) {
        this.mDeviceNameFlag = mDeviceNameFlag;
    }

    public boolean isCanOneTouchOpen(String pin) {
        ZKTool.saveCrashInfo2File("Into isCanOneTouchOpen : mDeviceType = " + mDeviceType);
        if (mDeviceType == null) return false;
        switch (mDeviceType) {
            case MA300:
            case IX8:
                return false;    //用的是另外一个判断
            case BIO_SWITCH:
            case BIO_LOCK:
            default:
                ZKTool.saveCrashInfo2File("Into isCanOneTouchOpen : pin = " + pin);
                if (pin != null && pin.length() != 0) {
                    ZKLog.d(TAG, "isCanOneTouchOpen: pin= " + pin);
                    return true;
                }
                break;
        }
        return false;
    }

    public boolean connect() {
        if (ZKBluetoothFactory.getBluetoothManagerBLE() != null) {
            return ZKBluetoothFactory.getBluetoothManagerBLE().connectBlock(this);
        } else return false;
    }
}
