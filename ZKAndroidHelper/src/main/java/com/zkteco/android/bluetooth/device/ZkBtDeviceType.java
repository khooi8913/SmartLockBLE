package com.zkteco.android.bluetooth.device;

import android.content.Context;

import com.zkteco.android.R;

/**
 * Created by xqc on 2015/9/1.
 * ZKTeco app
 */
public enum ZkBtDeviceType {

    UNKNOWN_DEVICE(0x0101, R.string.unknownDevice),

    BIO_SWITCH(0x1101, R.string.bioSwitch), BIO_LOCK(0x1110, R.string.bioLock),

    IX8(0x1001, R.string.ix8), MA300(0x1002, R.string.ma300);


    private int mNameResId;
    private int mDeviceTypeCode;

    ZkBtDeviceType(int deviceTypeCode, int nameResId) {
        this.mDeviceTypeCode = deviceTypeCode;
        this.mNameResId = nameResId;
    }

    public static String getDeviceNameByCode(int code) {
        return getDeviceTypeByCode(code).name();
    }

    public static ZkBtDeviceType getDeviceTypeByCode(int code) {
        for (ZkBtDeviceType type : ZkBtDeviceType.values()) {
            if (type.mDeviceTypeCode == code) {
                return type;
            }
        }
        return UNKNOWN_DEVICE;
    }

    public int getDeviceTypeCode() {
        return mDeviceTypeCode;
    }

    public String getName(Context context) {
        return context.getString(mNameResId);
    }
}
