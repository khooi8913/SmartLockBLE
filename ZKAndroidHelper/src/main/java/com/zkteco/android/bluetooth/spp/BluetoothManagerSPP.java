package com.zkteco.android.bluetooth.spp;


import android.content.Context;

import com.zkteco.android.bluetooth.ZKBluetoothManager;

public class BluetoothManagerSPP extends ZKBluetoothManager {

    public BluetoothManagerSPP(Context context) {
        super(context);
    }

    public int read(byte[] buffer, int offset, int count) {
        return -2;
    }

    public int write(byte[] buffer, int offset, int count) {
        return -2;
    }
}
