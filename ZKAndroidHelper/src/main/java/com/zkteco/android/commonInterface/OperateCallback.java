package com.zkteco.android.commonInterface;

/**
 * Created by xqc on 2015/9/14.
 * ZKTeco app
 */
public interface OperateCallback {
    void operateFinish(boolean isSucceed);
}
