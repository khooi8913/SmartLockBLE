package com.zkteco.android.dialog;

import android.content.Context;
import android.os.Build;
import android.text.InputType;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.zkteco.android.R;

/**
 * Created by xqc on 2015/9/8.
 * ZKTeco app
 */

/**
 * 颜色由style中的属性设定
 * <!-- Used for the buttons -->
 * <item name="colorAccent">@color/lightGreen</item>
 * <!-- Used for the title and text -->
 * <item name="android:textColorPrimary">#FFFFFF</item>
 * <!-- Used for the background -->
 * <item name="android:background">#4CAF50</item>
 */

public class ZkDialogManager {
    /**
     * 经典dialog样式
     */
    public static MaterialDialog createClassicDialog(Context context,
                                                     String title,
                                                     String text,
                                                     String positiveText,
                                                     String negativeText,
                                                     MaterialDialog.ButtonCallback callback) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .callback(callback)
                .build();
    }

    public static MaterialDialog createToastDialog(Context context, String title, String text) {
        return createClassicDialog(context,
                title, text,
                context.getString(R.string.ok), "",
                null);
    }

    public static MaterialDialog createToastDialog(Context context, int title, int text) {
        return createClassicDialog(context,
                context.getString(title), context.getString(text),
                context.getString(R.string.ok), "",
                null);
    }

    public static MaterialDialog createToastDialog(Context context, int title, int text,
                                                   MaterialDialog.ButtonCallback callback) {
        return createClassicDialog(context,
                context.getString(title), context.getString(text),
                context.getString(R.string.ok), "",
                callback);
    }

    public static MaterialDialog createToastDialogCantCancel(Context context, int title, int text,
                                                             MaterialDialog.ButtonCallback callback) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .positiveText(R.string.ok)
                .negativeText("")
                .cancelable(false)
                .callback(callback)
                .build();
    }

    public static MaterialDialog createConfirmDialog(Context context, String title, String text,
                                                     MaterialDialog.ButtonCallback callback) {
        return createClassicDialog(context,
                title, text,
                context.getString(R.string.ok), context.getString(R.string.cancel),
                callback
        );
    }

    /**
     * 不可取消
     */
    public static MaterialDialog createConfirmDialog(Context context, int title, int text,
                                                     MaterialDialog.ButtonCallback callback) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .callback(callback)
                .cancelable(false)
                .build();
    }

    /**
     * 圆形加载dialog
     */
    public static MaterialDialog createCircleProgressDialog(Context context, String text) {

        return new MaterialDialog.Builder(context)
                .content(text)
                .progress(true, 0)
                .cancelable(false)
                .build();
    }

    public static MaterialDialog createCircleProgressDialog(Context context, String text, boolean cancelable) {

        return new MaterialDialog.Builder(context)
                .content(text)
                .progress(true, 0)
                .cancelable(cancelable)
                .build();
    }

    public static MaterialDialog createCircleProgressDialog(Context context, int textResId, boolean cancelable) {
        return createCircleProgressDialog(context, context.getString(textResId), cancelable);
    }


    public static MaterialDialog createCircleProgressDialog(Context context, int textResId) {
        return createCircleProgressDialog(context, context.getString(textResId));
    }

    /**
     * 输入dialog
     */
    public static MaterialDialog createInputDialog(Context context,
                                                   String title,
                                                   String text,
                                                   int inputType,
                                                   int minLength,
                                                   int maxLength,
                                                   String hint,
                                                   String preFill,
                                                   MaterialDialog.InputCallback inputCallback) {
        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .inputType(inputType)
                .inputRange(minLength, maxLength)
                .input(hint, preFill, false, inputCallback)
                .build();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            EditText editText = dialog.getInputEditText();
            if (editText != null) {
                editText.setBackgroundResource(R.drawable.edittext_bg_activated);
            }
        }
        return dialog;
    }

    public static MaterialDialog createDigitalPwdInputDialog(Context context,
                                                             String title,
                                                             String text,
                                                             int length,
                                                             MaterialDialog.InputCallback inputCallback) {
        int inputType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD;
        return createInputDialog(context,
                title, text,
                inputType, length, length,
                text, "", inputCallback);
    }

    public static MaterialDialog createInputDialog(Context context,
                                                   String title,
                                                   String text,
                                                   int maxLength,
                                                   MaterialDialog.InputCallback inputCallback) {
        return createInputDialog(context, title, text, 0, 1, maxLength, text, "", inputCallback);
    }

    public static MaterialDialog createInputDialog(Context context,
                                                   int title,
                                                   int text,
                                                   int maxLength,
                                                   MaterialDialog.InputCallback inputCallback) {
        return createInputDialog(context,
                context.getString(title), context.getString(text),
                -1, 1, maxLength,
                context.getString(text), "", inputCallback);
    }

    /**
     * 单选dialog
     */
    public static MaterialDialog createSingleChoiceListDialog(Context context,
                                                              int title,
                                                              int itemRes,
                                                              int selectIndex,
                                                              MaterialDialog.ListCallbackSingleChoice callBackSingleChoice,
                                                              int positiveResId) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .items(itemRes)
                .itemsCallbackSingleChoice(selectIndex, callBackSingleChoice)
                .positiveText(positiveResId)
                .build();
    }

    public static MaterialDialog createSingleChoiceListDialog(Context context,
                                                              int title,
                                                              String[] items,
                                                              int selectIndex,
                                                              MaterialDialog.ListCallbackSingleChoice callBackSingleChoice,
                                                              int positiveResId) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .items(items)
                .itemsCallbackSingleChoice(selectIndex, callBackSingleChoice)
                .positiveText(positiveResId)
                .build();
    }

    public static MaterialDialog createAlwaysCallInputDialog(Context context, int title, int text,
                                                             int maxLength, String preFill,
                                                             MaterialDialog.InputCallback inputCallback,
                                                             MaterialDialog.SingleButtonCallback positiveCallback) {
        return new MaterialDialog.Builder(context)
                .title(title).content(text)
                .inputRange(1, maxLength)
                .alwaysCallInputCallback()
                .input("", preFill, inputCallback)
                .onPositive(positiveCallback)
                .build();
    }
}
