package com.zkteco.android.event;

public interface AdCallbackListener {
	public void onSplashError(String error);
	public void onSplashClose();
	public void onSplashSucceed();
	public void onSplashClickAd(String adName);
	public void onSplashRealClickAd(String adName);
}
