package com.zkteco.android.event;

import android.view.View;

public interface OnRecyclerViewItemClickListener {
    void onItemClick(View view, int position);
}