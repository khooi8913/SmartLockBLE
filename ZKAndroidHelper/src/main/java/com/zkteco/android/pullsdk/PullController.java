package com.zkteco.android.pullsdk;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

public class PullController {
    private static final String TAG = PullController.class.getSimpleName();

    public static final int STATE_PULL_NONE = 3;
    public static final int STATE_PULL_CONNECTED = 5;
    public static final int ERROR_WRONG_PWD = -14;

    private static BluetoothDevice mBluetoothDevice;
    private static int mPullState;
    public static long mPullHandle;

    public static long connectPullByBluetooth(String Password, int timeOut) {
        long ret = PullHelper.connectPull("BLUETOOTH", "", 0, timeOut, Password);
        upDateStateByRet(ret);
        return ret;
    }

    public static long connectPullByBluetooth(BluetoothDevice device) {
        mBluetoothDevice = device;
        long ret = PullHelper.connectPull("BLUETOOTH", device.getAddress(), 4370, 2000, "1234");
        upDateStateByRet(ret);
        return ret;
    }

    public static void upDateStateByRet(long ret) {
        Log.d(TAG, "updatePullState    ret = " + ret);
        if (ret != 0) {
            mPullState = STATE_PULL_CONNECTED;
            mPullHandle = ret;
        } else {
            mPullState = STATE_PULL_NONE;
            mPullHandle = 0;
        }
    }

    public static long getPullHandle() {
        return mPullHandle;
    }

    public static void setBluetoothDevice(BluetoothDevice device) {
        mBluetoothDevice = device;
    }

    public static boolean shoutDownExternalDevice() {
        Log.d(TAG, "start shoutDownDevice               my pull state is " + mPullHandle);
        if (mPullState != STATE_PULL_CONNECTED) return false;
//            connectPullByBluetooth(mBluetoothDevice);
        int ret = PullSDK.ControlDevice(mPullHandle, 1, 0, 0, 6, 0, "");
        Log.d(TAG, "shoutDownDevice  " + "   ans ret =" + ret);
        return ret == 0;
    }

    public static void cleanPullState() {
        mPullHandle = 0;
        mPullState = STATE_PULL_NONE;
    }

    public static boolean setSleepTime(int time) {
        Log.d(TAG, "start set sleep time               my pull state is " + mPullState);
        if (mPullState != STATE_PULL_CONNECTED) {
//            connectPullByBluetooth(mBluetoothDevice);
            return false;
        }
        String CMD = "sys.autopower=" + time * 60;
        int ret = PullSDK.SetDeviceParam(mPullHandle, CMD);
        Log.d(TAG, "setSleepTime CMD is " + CMD + "      ans ret =" + ret);
        return ret == 0;
    }

    public static void disconnect(long handle) {
        Log.d(TAG, "start pull disconnect              my pull state is " + mPullState);
        if (mPullState != STATE_PULL_CONNECTED) {
            return;
        }
        PullSDK.Disconnect(handle);
        mPullState = STATE_PULL_NONE;
    }

    /**
     * 拉取连接错误码
     *
     * @return 连接错误码
     */
    public static int getPullLastError() {
        return PullSDK.PullLastError();
    }

    public static boolean isWrongPullPwd() {
        return getPullLastError() == ERROR_WRONG_PWD;
    }
}
