package com.zkteco.android.pullsdk;

import com.zkteco.android.bluetooth.ZKBluetoothFactory;
import com.zkteco.android.tool.ZKLog;
import com.zkteco.android.tool.ZKTool;


public class PullHelper {
    private static final String TAG = "PullHelper";

    private static final int DEFAULT_BUFFER_LENGTH = 4096;
    private static final int DEFAULT_BUFFER_SIZE_LENGTH = 8;

    public static final int PULL_CONNECT_ERROR = -2;
    public static final int STATE_PULL_NONE = 3;
    public static final int STATE_PULL_CONNECTING = 4;
    public static final int STATE_PULL_CONNECTED = 5;

    private static int mPullState;
    private static int lastRet = 0;

    private static void cleanPullDataBuffer() {
        ZKBluetoothFactory.getZKBluetoothManager().cleanPullDataBuffer();
    }

    public static long connectPull(String protocol, String address, int port, int timeOut, String password) {
        ZKLog.d(TAG, "connecting pull");
        upDataState(STATE_PULL_CONNECTING);
        String parameters =
//                "protocol=" + protocol + ",address=" + address + ",port=" + port + ",timeout=" + timeOut + ",passwd=" + password;
                "protocol=" + protocol + ",address=" + address + ",timeout=" + timeOut + ",passwd=" + password;
        ZKLog.d(TAG, "my parameters is : " + parameters);
        long handle = 0;
        try {
            handle = PullSDK.Connect(parameters);
        } catch (Exception e) {
            ZKLog.e(TAG, "pull connect error");
            handle = 0;
        }
        ZKLog.d(TAG, "My PULL HANDLE IS : " + handle);
        if (handle != 0) {
            upDataState(STATE_PULL_CONNECTED);
            return handle;
        } else {
            upDataState(STATE_PULL_NONE);
            int errorCode = PullSDK.PullLastError();
            ZKLog.e(TAG, "PULL ERROR CODE IS : " + errorCode);
            return 0;
        }
    }

    public static void upDataState(int state) {
        ZKLog.d(TAG, "setPullState() " + mPullState + " -> " + state);
        mPullState = state;
    }

    public static int getPullState() {
        return mPullState;
    }

    public static int
    setDeviceFileData(long handle, String Filename, byte[] Buffer, int BufferSize, String Options) {
        try {
            lastRet = PullSDK.SetDeviceFileData(handle, Filename, Buffer, BufferSize, Options);
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "setDeviceFileData error", e);
        }
        return lastRet;
    }

    public static int
    getDeviceFileData(long handle, byte[] Buffer, int[] BufferSize, String Filename, String Options) {
        try {
            lastRet = PullSDK.GetDeviceFileData(handle, Buffer, BufferSize, Filename, Options);
            ZKLog.d(TAG, "my get device file data ret is :" + lastRet);
            ZKLog.d(TAG, "my get device file data buffer size is :" + BufferSize[0]);
            if (lastRet < 0) BufferSize[0] = 0;
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "getDeviceFileData error", e);
        }
        return lastRet;
    }


    public static int
    SetDeviceParam(long handle, String ItemAndValues) {
        try {
            lastRet = PullSDK.SetDeviceParam(handle, ItemAndValues);
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "setDeviceFileData error", e);
        }
        return lastRet;
    }

    public static int
    getDeviceParam(long handle, byte[] buffer, int[] bufferSize, String items) {
        try {
            lastRet = PullSDK.GetDeviceParam(handle, buffer, bufferSize, items);
            ZKLog.d(TAG, "my get device param ret is :" + lastRet);
            ZKLog.d(TAG, "my get device param buffer size is :" + bufferSize[0]);
//            ZKLog.d(TAG, ZKTool.getHexString(buffer,0,bufferSize[0],true));
            if (lastRet < 0) bufferSize[0] = 0;
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "getDeviceParam error", e);
        }
        return lastRet;
    }

    public static String
    GetTableStruct(long handle) {
        cleanPullDataBuffer();
        byte[] buffer = new byte[DEFAULT_BUFFER_LENGTH];
        int[] bufferSize = new int[DEFAULT_BUFFER_SIZE_LENGTH];
        bufferSize[0] = buffer.length;
        try {
            lastRet = PullSDK.GetTableStruct(handle, buffer, bufferSize);
            ZKLog.d(TAG, "my get device Table Struct ret is :" + lastRet);
            ZKLog.d(TAG, "my get device Table Struct buffer size is :" + bufferSize[0]);
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "GetTableStruct error", e);
        }

        String bufferString = new String(buffer).trim();
        ZKLog.d(TAG, bufferString);
        return lastRet == 0 ? bufferString : "";
    }

    public static int
    SetTableStruct(long handle, String tableStruct) {
        try {
            lastRet = PullSDK.SetTableStruct(handle, tableStruct);
            ZKLog.d(TAG, "set Table Struct ret is : " + lastRet);
        } catch (Exception e) {
            lastRet = -1;
            ZKLog.e(TAG, "Set Table Struct error", e);
        }
        return lastRet;
    }

    public static int
    GetDeviceData(long handle, byte[] buffer, int[] bufferSize, String tableName, String fieldNames, String filter, String options) {
        try {
            lastRet = PullSDK.GetDeviceData(handle, buffer, bufferSize, tableName, fieldNames, filter, options);
            ZKLog.d(TAG, "my get device data ret is :" + lastRet);
            ZKLog.d(TAG, "my get device data buffer size is :" + bufferSize[0]);
//            ZKLog.d(TAG, ZKTool.getHexString(buffer,0,bufferSize[0],true));
            if (lastRet < 0) bufferSize[0] = 0;
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "GetDeviceData error", e);
        }
        return lastRet;
    }


    public static int
    SetDeviceData(long handle, String tableName, String data, String options) {
        try {
            lastRet = PullSDK.SetDeviceData(handle, tableName, data, options);
            ZKLog.d(TAG, "my set device data ret is :" + lastRet);
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "SetDeviceData error", e);
        }
        return lastRet;
    }

    public static int
    DeleteDeviceData(long handle, String tableName, String datas, String options) {
        try {
            lastRet = PullSDK.DeleteDeviceData(handle, tableName, datas, options);
            ZKLog.d(TAG, "my delete device data ret is :" + lastRet);
        } catch (Exception e) {
            lastRet = PULL_CONNECT_ERROR;
            ZKLog.e(TAG, "DeleteDeviceData error", e);
        }
        return lastRet;
    }

    public static int controlDevice(long handle, long operationId, long param1, long param2, long param3, long param4, String options) {
        if (options == null) {
            options = "";
        }
        ZKLog.d(TAG, "start control Device ---the opera data is " +
                operationId + "." + param1 + "." + param2 + "." + param3 + "." + param4 + "." + options);
        lastRet = PullSDK.ControlDevice(handle, operationId, param1, param2, param3, param4, options);
        ZKLog.d(TAG, "control device ret is " + lastRet);
        return lastRet;
    }

    public static int getDeviceFileDataHelper(long handle, byte[] buffer, String fileName, String options) {
        cleanPullDataBuffer();
        int[] bufferSize = new int[DEFAULT_BUFFER_SIZE_LENGTH];
        bufferSize[0] = buffer.length;

        lastRet = PullHelper.getDeviceFileData(handle, buffer, bufferSize, fileName, options);
        if (ZKTool.DEBUG) {
//            String bufferString = new String(buffer).trim();
//            ZKLog.d(TAG, bufferString);
            String bf = ZKTool.getHexString(buffer, 0, bufferSize[0], true);
            ZKLog.d(TAG, bf);
        }
        return lastRet;
    }


    public static int getDeviceDataHelper(long handle, byte[] buffer,
                                          String tableName, String fieldName, String filter,
                                          String options) {
        cleanPullDataBuffer();
        int[] bufferSize = new int[DEFAULT_BUFFER_SIZE_LENGTH];
        bufferSize[0] = buffer.length;

        lastRet = GetDeviceData
                (handle, buffer, bufferSize, tableName, fieldName, filter, options);

        if (ZKTool.DEBUG) {
            String bufferString = new String(buffer).trim();
            ZKLog.d(TAG, "data : " + bufferString);
            ZKLog.d(TAG, "data count =" + lastRet);
        }

        return lastRet;
    }

    public static int getDeviceParamHelper(long handle, byte[] buffer, String items) {
        cleanPullDataBuffer();
        int[] bufferSize = new int[DEFAULT_BUFFER_SIZE_LENGTH];
        bufferSize[0] = buffer.length;

        lastRet = getDeviceParam(handle, buffer, bufferSize, items);

        if (ZKTool.DEBUG) {
            String bufferString = new String(buffer).trim();
            ZKLog.d(TAG, "data : " + bufferString);
            ZKLog.d(TAG, "data count =" + lastRet);
        }
        return lastRet;
    }

    public static int getLastRet() {
        if (PullController.mPullHandle != 0) {
            return lastRet;
        } else return 0;
    }

    /**
     * 设置udk转码格式
     *
     * @param charset 1：GBK
     *                other：UTF-8
     */
    public static void setCharset(int charset) {
        ZKLog.d(TAG, "setCharset: " + charset);
        PullSDK.SetCharset(charset);
    }
}


