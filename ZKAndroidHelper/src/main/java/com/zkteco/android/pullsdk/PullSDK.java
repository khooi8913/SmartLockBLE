package com.zkteco.android.pullsdk;

public class PullSDK {

    static {
        System.loadLibrary("pullSDK");
    }

    public static native long
    Connect(String parameters);

    public static native void
    Disconnect(long handle);

    public static native int
    ControlDevice(long handle, long OperationID, long Param1, long Param2, long Param3, long Param4, String Options);

    public static native int
    GetDeviceParam(long handle, byte[] Buffer, int[] BufferSize, String Items);

    public static native int
    SetDeviceParam(long handle, String ItemAndValues);

    public static native int
    GetRTLog(long handle, byte[] Buffer, int[] BufferSize);

    public static native int
    GetDeviceData(long handle, byte[] Buffer, int[] BufferSize, String TableName, String FieldNames, String Filter, String Options);

    public static native int
    SetDeviceData(long handle, String TableName, String Datas, String Options);

    public static native int
    DeleteDeviceData(long handle, String TableName, String Datas, String Options);

    public static native int
    GetDeviceDataCount(long handle, String TableName, String Filter, String Options);

    public static native int
    SetDeviceFileData(long handle, String Filename, byte[] Buffer, int BufferSize, String Options);

    public static native int
    GetDeviceFileData(long handle, byte[] Buffer, int[] BufferSize, String Filename, String Options);

    public static native int
    SearchDevice(String commtype, String address, byte[] Buffer, int[] BufferSize);

    public static native int
    ModifyIPAddress(String commtype, String address, String Buffer);

    public static native int
    GetTableStruct(long handle, byte[] Buffer, int[] BufferSize);

    public static native int
    SetTableStruct(long handle, String TableStruct);

    public static native int
    SetAppid(long handle, String appid);

    public static native int
    PullLastError();

    public static native void
    SetCharset(int nChartset);
}
