package com.zkteco.android.tool;

import android.util.Log;

public class ZKLog {
	/** Log output level */
	public enum LEVEL {

		VERBOSE(VERBOSE_LEVEL), DEBUG(DEBUG_LEVEL), INFO(INFO_LEVEL), WARN(WARN_LEVEL), ERROR(
				ERROR_LEVEL);

		private int level;

		private LEVEL(final int level) {
			this.level = level;
		}

		public int getLevel() {
			return this.level;
		}
	}

	//private static final String TAG = ZKLog.class.getName();

	private static final int VERBOSE_LEVEL = 1;
	
	private static final int DEBUG_LEVEL = 2;

	private static final int INFO_LEVEL = 3;

	private static final int WARN_LEVEL = 4;

	private static final int ERROR_LEVEL = 5;

	public static int sLevel = LEVEL.VERBOSE.getLevel();

	public static void v(String tag, String msg) {
		if (ZKTool.DEBUG && sLevel < DEBUG_LEVEL) {
			Log.v(tag, msg);
			if (ZKTool.SD_LOG) {
			}
		}
	}

	public static void d(String tag, String msg) {
		if (ZKTool.DEBUG && sLevel < INFO_LEVEL) {
			Log.d(tag, msg);
			if (ZKTool.SD_LOG) {
			}
		}
	}
	
	public static void i(String tag, String msg) {
		if (ZKTool.DEBUG && sLevel < WARN_LEVEL) {
			Log.i(tag, msg);
			if (ZKTool.SD_LOG) {
			}
		}
	}

	public static void w(String tag, String msg) {
		if (ZKTool.DEBUG && sLevel < ERROR_LEVEL) {
			Log.w(tag, msg);
			if (ZKTool.SD_LOG) {
			}
		}
	}

	public static void w(String tag, String msg, Throwable tr) {
		if (ZKTool.DEBUG && sLevel < ERROR_LEVEL) {
			Log.w(tag, msg, tr);
			if (ZKTool.SD_LOG) {
			}
		}
	}

	public static void e(String tag, String msg) {
		if (ZKTool.DEBUG) {
			Log.e(tag, msg);
			if (ZKTool.SD_LOG) {
			}
		}
	}

	public static void e(String tag,String msg,Exception e){
		if (ZKTool.DEBUG){
			Log.e(tag,msg,e);
			if (ZKTool.SD_LOG) {
			}
		}
	}
}
