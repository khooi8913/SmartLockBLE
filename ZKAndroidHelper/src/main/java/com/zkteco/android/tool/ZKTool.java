package com.zkteco.android.tool;

import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.List;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class ZKTool {
    //developer set true,market need to set false
    public final static boolean DEBUG = false;
    //set whether can write to file
    public final static boolean SD_LOG = true;
    //是否启动蒲公英调试工具
    public static final boolean ENABLE_TEST_HELPER = false;

    public static final String TAG = ZKTool.class.getName();
    private static DisplayMetrics displayMetrics = new DisplayMetrics();

    public static boolean checkStringNull(String s) {
        if (s == null || "".equals(s)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 产生随机数 取值范围(0 --maxvalue-1)函(0 ,maxvalue-1) Running
     *
     * @param maxvalue
     * @return <p/>
     * Create time: 2012-8-12下午2:03:01
     */
    public static int rand(int maxvalue) {
        Random random = new Random(System.currentTimeMillis());
        return (random.nextInt() << 1 >>> 1) % maxvalue;
    }

    /**
     * 计算屏幕尺寸 Create time: 2012-9-13上午9:21:46
     *
     * @param act
     */
    public static void displaySize(Activity act) {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(dm);
        //int width = dm.widthPixels;
        //int height = dm.heightPixels;
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, double dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 调用系统浏览器
     */
    public static void openSystemBrowser(Context context, String url)
            throws Exception {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri content_uri_browsers = Uri.parse(url);
            intent.setData(content_uri_browsers);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
        }
    }

    /**
     * 通过反射机制调用类方法 <br>
     * Create time: 2014-2-7上午10:22:32
     *
     * @param c
     * @param methodName
     * @param obj
     * @param paramTypes
     * @param params
     */
    public static void call(Class<?> c, String methodName, Object obj,
                            Class<?>[] paramTypes, Object... params) {
        try {
            Method method = c.getMethod(methodName, paramTypes);
            method.setAccessible(true);
            method.invoke(obj, params);
        } catch (Throwable e) {
        }
    }

    /***
     * 判断当前系统的语言环境是否为中文语言环境
     */
    public static boolean isChineseLanguageEnvironment(Context context) {

        String language = context.getResources().getConfiguration().locale.getLanguage();
        if ("zh".equals(language)) {
            return true;
        }
        return false;
    }

    /***
     * 线程等待
     */
    public static void threadSleep(int time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean canParse2Int(String string, int length) {
        if (length != 0 && string.length() != length) return false;
        try {
            int i = Integer.parseInt(string);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getHexString(byte[] b, int offset, int count, boolean haveSpace) {
        String string = "";
        for (int i = offset; i < offset + count; i++) {
            String hex = Integer.toHexString(b[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            if (haveSpace) hex += " ";
            string = string + hex.toUpperCase();
        }
        return string;
    }

    public static String getHexString(byte[] data, boolean isHaveSpace) {
        final StringBuilder stringBuilder = new StringBuilder(data.length);
        for (byte byteChar : data) {
            stringBuilder.append(String.format("%02X", byteChar));
            if (isHaveSpace) stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public static final String ALGORITHM_DES = "DES/CBC/PKCS5Padding";

    public static String encryptDES(String data, String key) {
        if (data == null) return null;
        try {
            DESKeySpec dks = new DESKeySpec(key.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            // key的长度不能够小于8位字节
            Key secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
            IvParameterSpec iv = new IvParameterSpec("12345678".getBytes());
            AlgorithmParameterSpec paramSpec = iv;
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);
            byte[] bytes = cipher.doFinal(data.getBytes());
            return byte2String(bytes);
        } catch (Exception e) {
            Log.e(TAG, "encryptDES ERROR", e);
            return data;
        }
    }

    public static String decryptDES(String data, String key) {
        if (data == null)
            return null;
        try {
            DESKeySpec dks = new DESKeySpec(key.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            // key的长度不能够小于8位字节
            Key secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
            IvParameterSpec iv = new IvParameterSpec("12345678".getBytes());
            AlgorithmParameterSpec paramSpec = iv;
            cipher.init(Cipher.DECRYPT_MODE, secretKey, paramSpec);
            return new String(cipher.doFinal(byte2hex(data.getBytes())));
        } catch (Exception e) {
            e.printStackTrace();
            return data;
        }
    }

    private static String byte2String(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String sTmp;
        for (int n = 0; b != null && n < b.length; n++) {
            sTmp = Integer.toHexString(b[n] & 0XFF);
            if (sTmp.length() == 1)
                hs.append('0');
            hs.append(sTmp);
        }
        return hs.toString().toUpperCase();
    }

    private static byte[] byte2hex(byte[] b) {
        if ((b.length % 2) != 0)
            throw new IllegalArgumentException();
        byte[] b2 = new byte[b.length / 2];
        for (int n = 0; n < b.length; n += 2) {
            String item = new String(b, n, 2);
            b2[n / 2] = (byte) Integer.parseInt(item, 16);
        }
        return b2;
    }

    public static int getScreenHeight(Activity activity) {
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static int getScreenWidth(Activity activity) {
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    /**
     * 在application中注册日志组件
     *
     * @param application application
     */
    public static void regCrashLog(Application application) {
//        if (ENABLE_TEST_HELPER) {
//            //蒲公英
//            PgyCrashManager.register(application);
//        }
    }

    /**
     * 在activity中注册摇一摇
     * 需要注销（resume pause）
     *
     * @param activity 注册的activity
     * @param enable   使能
     */
    public static void regFeedbackShake(Activity activity, boolean enable) {
//        if (ENABLE_TEST_HELPER) {
//            // 自定义摇一摇的灵敏度，默认为950，数值越小灵敏度越高。
//            PgyFeedbackShakeManager.setShakingThreshold(1000);
//            // 以对话框的形式弹出
//            if (enable) {
//                PgyFeedbackShakeManager.register(activity);
//            } else {
//                PgyFeedbackShakeManager.unregister();
//            }
//        }
    }

    /**
     * 检查更新
     *
     * @param activity activity
     */
    public static void checkUpdate(Activity activity) {
//        if (ENABLE_TEST_HELPER) {
//            //蒲公英（检查更新）
//            PgyUpdateManager.register(activity);
//        }
    }


    private static boolean checkIsWifiNet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            }
        }
        return false;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        if (listView == null) return;

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static boolean isEmpty(List list) {
        return list != null && list.size() == 0;
    }


    public static String saveCrashInfo2File(String logInfo) {
        return "";
//        StringBuffer sb = new StringBuffer();
//
//        String result = logInfo + "\n";
//        sb.append(result);
//        try {
//            String fileName = "log.txt";
//            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
////				String path = "/sdcard/crash/";
//                String path = Environment.getExternalStorageDirectory().getPath()
//                        + "/android.com.zkbiobt/TestLog/";
//                File dir = new File(path);
//                if (!dir.exists()) {
//                    dir.mkdirs();
//                }
//                FileOutputStream fos = new FileOutputStream(path + fileName,true);
//                fos.write(sb.toString().getBytes());
//                fos.close();
//            }
//            return fileName;
//        } catch (Exception e) {
//            Log.e(TAG, "an error occured while writing file...", e);
//        }
//        return null;
    }
}
