package com.zkteco.android.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.zkteco.android.R;
import com.zkteco.android.tool.ZKLog;
import com.zkteco.android.tool.ZKTool;

/**
 * Created by xqc on 2015/10/29.
 * ZKTeco App
 */
public class ClearAbleEditText extends EditText
        implements View.OnTouchListener, View.OnFocusChangeListener, TextWatcher {
    private static final String TAG = "ClearAbleEditText";

    private Context mContext;
    private Drawable mClearTextIcon;
    private OnFocusChangeListener mOnFocusChangeListener;
    private OnTouchListener mOnTouchListener;
    private OnTextCleanListener mOnTextCleanListener;

    public ClearAbleEditText(final Context context) {
        super(context);
        init(context);
    }

    public ClearAbleEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ClearAbleEditText(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    public void setOnFocusChangeListener(final OnFocusChangeListener onFocusChangeListener) {
        mOnFocusChangeListener = onFocusChangeListener;
    }

    @Override
    public void setOnTouchListener(final OnTouchListener onTouchListener) {
        mOnTouchListener = onTouchListener;
    }

    private void init(final Context context) {
        mContext = context;
        setBackgroundResource(R.drawable.shape_without_bg);
        setStartIcon(context, R.drawable.icon_search_magnifier);

        final Drawable drawable = ContextCompat.getDrawable(context, R.drawable.abc_ic_clear_mtrl_alpha);
        final Drawable wrappedDrawable = DrawableCompat.wrap(drawable); //Wrap the drawable so that it can be tinted pre Lollipop
        DrawableCompat.setTint(wrappedDrawable, getCurrentHintTextColor());
        mClearTextIcon = wrappedDrawable;
        mClearTextIcon.setBounds(0, 0, mClearTextIcon.getIntrinsicHeight(), mClearTextIcon.getIntrinsicHeight());

        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(this);

        setParam(context);
    }

    public void setStartIcon(Context context, int resId) {
        final Drawable res = ContextCompat.getDrawable(context, resId);
        final Drawable drawable = DrawableCompat.wrap(res);
        drawable.setBounds(0, 0, drawable.getIntrinsicHeight(), drawable.getIntrinsicHeight());
        setCompoundDrawables(drawable, null, null, null);
    }

    private void setParam(Context context) {
        setCompoundDrawablePadding(ZKTool.dip2px(context, 10));
        setPaddingByIcon(context, false);

        setSingleLine();
        setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        setInputType(EditorInfo.TYPE_CLASS_TEXT);

        setHintTextColor(getResources().getColor(R.color.light_gray));
    }

    private void setPaddingByIcon(Context context, boolean isVisibility) {
        setPadding(ZKTool.dip2px(context, 5),
                ZKTool.dip2px(context, 5),
                ZKTool.dip2px(context, isVisibility ? 3 : 35),
                ZKTool.dip2px(context, 5));
    }

    @Override
    public void onFocusChange(final View view, final boolean hasFocus) {
        ZKLog.d(TAG, "onFocusChange: hasFocus:" + hasFocus);
        if (hasFocus) {
            setClearIconVisible(getText().length() > 0);
        } else {
            setClearIconVisible(false);
        }
        if (mOnFocusChangeListener != null) {
            mOnFocusChangeListener.onFocusChange(view, hasFocus);
        }
    }

    @Override
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        final int x = (int) motionEvent.getX();
        if (mClearTextIcon.isVisible() && x > getWidth() - getPaddingRight() - mClearTextIcon.getIntrinsicWidth()) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                setError(null);
                setText("");
            }
            return true;
        }
        return mOnTouchListener != null && mOnTouchListener.onTouch(view, motionEvent);
    }

    @Override
    public final void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        ZKLog.d(TAG, "onTextChanged: change");
        if (isFocused()) {
            setClearIconVisible(s.length() > 0);
            if (s.length() == 0 && mOnTextCleanListener != null) {
                ZKLog.d(TAG, "onTextChanged: clean");
                mOnTextCleanListener.OnTextBeenClean();
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }


    private void setClearIconVisible(final boolean visible) {
        mClearTextIcon.setVisible(visible, false);
        final Drawable[] compoundDrawables = getCompoundDrawables();
        setCompoundDrawables(
                compoundDrawables[0],
                compoundDrawables[1],
                visible ? mClearTextIcon : null,
                compoundDrawables[3]);
        setPaddingByIcon(mContext, visible);
    }

    public void setOnTextCleanListener(OnTextCleanListener onTextCleanListener) {
        mOnTextCleanListener = onTextCleanListener;
    }

    public interface OnTextCleanListener {
        void OnTextBeenClean();
    }

    public void closeKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        clearFocus();
    }
}