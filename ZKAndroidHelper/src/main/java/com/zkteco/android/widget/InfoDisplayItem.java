package com.zkteco.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zkteco.android.R;

/**
 * Created by xqc on 2015/11/27.
 * ZKTeco App
 */
public class InfoDisplayItem extends LinearLayout {
    private Context mContext;
    private TextView mTitle;
    private TextView mContent;

    public InfoDisplayItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater.from(mContext).inflate(R.layout.view_item_info_display, this, true);

        mTitle = (TextView) findViewById(R.id.info_display_title);
        mContent = (TextView) findViewById(R.id.info_display_content);

        initView(attrs);
    }

    private void initView(AttributeSet attrs) {
        TypedArray attrArray = mContext.obtainStyledAttributes(attrs, R.styleable.InfoDisplayItem);
        int count = attrArray.getIndexCount();
        for (int i = 0; i < count; i++) {
            int attrName = attrArray.getIndex(i);

            //标题
            if (attrName == R.styleable.InfoDisplayItem_infoDisTitle) {
                mTitle.setText(attrArray.getString(attrName));
                continue;
            }
            //内容
            if (attrName == R.styleable.InfoDisplayItem_infoDisContent) {
                mContent.setText(attrArray.getString(attrName));
                continue;
            }
        }
        attrArray.recycle();
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setTitle(int ResId) {
        mTitle.setText(ResId);
    }

    public void setContent(String content) {
        mContent.setText(content);
    }

    public void setContent(int resId) {
        mContent.setText(resId);
    }
}
