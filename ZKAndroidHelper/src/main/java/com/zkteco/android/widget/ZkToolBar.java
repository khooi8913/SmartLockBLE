package com.zkteco.android.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zkteco.android.R;

/**
 * Created by xqc on 2015/10/30.
 * ZKTeco App
 */
public class ZkToolBar extends RelativeLayout {
    private static final String TAG = "ZkToolBar";

    private Context mContext;
    private RelativeLayout mView;
    private TextView mTitle;
    private ImageView mLeftBtn;
    private ImageView mRightBtn;
    private TextView mRightText;

    public ZkToolBar(final Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mView = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.view_item_toolbar, this, true);
        mView.setBackgroundColor(getResources().getColor(R.color.lightGreen));

        mTitle = (TextView) findViewById(R.id.toolbar_title);
        mLeftBtn = (ImageView) findViewById(R.id.toolbar_left_img);
        mRightBtn = (ImageView) findViewById(R.id.toolbar_right_img);
        mRightText = (TextView) findViewById(R.id.toolbar_right_tv);

        init(attrs);
        if (isInEditMode()) return;

        mLeftBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });
    }

    private void init(AttributeSet attrs) {
        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.ZkToolBar);
        for (int i = 0; i < a.getIndexCount(); i++) {
            int attr = a.getIndex(i);
            //标题
            if (attr == R.styleable.ZkToolBar_zkTbTitle) {
                mTitle.setText(a.getString(attr));
                //标题颜色
            } else if (attr == R.styleable.ZkToolBar_zkTbTitleColor) {
                mTitle.setTextColor(a.getColor(attr, getResources().getColor(R.color.black)));
                //左图像
            } else if (attr == R.styleable.ZkToolBar_zkTbLeftImg) {
                mLeftBtn.setImageResource(a.getResourceId(attr, R.drawable.ic_back));
                //右图像
            } else if (attr == R.styleable.ZkToolBar_zkTbRightImg) {
                mRightBtn.setImageResource(a.getResourceId(attr, 0));
                //右颜色
            } else if (attr == R.styleable.ZkToolBar_zkTbRightText) {
                mRightText.setText(a.getString(attr));
                //背景
            } else if (attr == R.styleable.ZkToolBar_zkTbBackground) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mView.setBackground(a.getDrawable(attr));
                } else mView.setBackgroundDrawable(a.getDrawable(attr));
                //阴影
            } else if (attr == R.styleable.ZkToolBar_zkTbElevation) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mView.setElevation(a.getDimensionPixelSize(attr, 0));
                }
                //是否显示右侧text
            } else if (attr == R.styleable.ZkToolBar_zkTbIsShowRightText) {
                setRightTextVisibility(a.getBoolean(attr, false) ? VISIBLE : GONE);
            }
        }
        a.recycle();
    }

    public void setTitle(String string) {
        mTitle.setText(string);
    }

    public void setTitle(int resId) {
        mTitle.setText(resId);
    }

    public void setLeftBtnRes(int resId) {
        mLeftBtn.setImageResource(resId);
    }

    public void setRightBtnRes(int resId) {
        mRightBtn.setImageResource(resId);
    }

    public void setRightText(String string) {
        mRightText.setText(string);
    }

    public void setRightText(int resId) {
        mRightText.setText(resId);
    }

    public void setRightBtnVisibility(int visibility) {
        mRightBtn.setVisibility(visibility);
        if (visibility == VISIBLE) {
            mRightText.setVisibility(GONE);
        }
    }

    public void setRightTextVisibility(int visibility) {
        mRightText.setVisibility(visibility);
        if (visibility == VISIBLE) {
            mRightBtn.setVisibility(GONE);
        }
    }

    public void setRightBtnListener(OnClickListener listener) {
        mRightBtn.setOnClickListener(listener);
    }

    public void setRightTextListener(OnClickListener listener) {
        mRightText.setOnClickListener(listener);
    }

    public void setLeftBtnListener(OnClickListener listener) {
        mLeftBtn.setOnClickListener(listener);
    }
}
