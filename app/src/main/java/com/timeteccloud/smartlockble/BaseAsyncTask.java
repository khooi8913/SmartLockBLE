package com.timeteccloud.smartlockble;

import android.content.Context;
import android.os.AsyncTask;

import com.zkteco.android.pullsdk.PullController;

/**
 * Created by TimeTec on 30-Nov-17.
 */

public class BaseAsyncTask <Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private static final String TAG = "BaseAsyncTask";

    private Context context;
    private long mPullHandle;

    public BaseAsyncTask(Context context){
        this.context = context;
        mPullHandle = PullController.getPullHandle();
    }

    @Override
    protected Result doInBackground(Params[] params) {
        return null;
    }
}
