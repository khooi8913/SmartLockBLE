package com.timeteccloud.smartlockble;

import android.bluetooth.BluetoothGattCharacteristic;

import com.zkteco.android.bluetooth.ZKBluetoothFactory;
import com.zkteco.android.bluetooth.ble.BluetoothManagerBLE;
import com.zkteco.android.tool.ZKTool;

/**
 * Created by xqc on 2015/11/12.
 * ZKTeco App
 */
public class BioBtATController {
    private static final String TAG = "BioBtATController";
    private static final int DEFAULT_WAIT_TIME = 5000;
    private static final int DEFAULT_SLEEP_TIME = 50;

    public static boolean pair(String pin) {
        return operate(BioLockATConstants.getVerifyPinString(pin), null,
                BioLockATConstants.ANS_PASSKEY_TRUE);
    }

    public static boolean changeName(String name) {
        return operate(null, BioLockATConstants.getLongNameChangeString(name),
                BioLockATConstants.ANS_OK);

    }

    public static boolean operate(String cmdString, byte[] cmdByte, String ansString) {
        BluetoothManagerBLE btManager = ZKBluetoothFactory.getBluetoothManagerBLE();
        final String[] ans = new String[1];
        btManager.setZKBleCharacteristicListener(new BluetoothManagerBLE.ZKBleCharacteristicListener() {
            @Override
            public void onCharacteristicChanged(int intentAction, BluetoothGattCharacteristic characteristic) {
                ans[0] = new String(characteristic.getValue());
            }
        });
        if (cmdString != null) {
            btManager.sendCMDString(cmdString);
        } else btManager.sendCMDByte(cmdByte);

        int iWait = 0;
        int iSleepTime = DEFAULT_SLEEP_TIME;
        while (iWait < DEFAULT_WAIT_TIME) {
            ZKTool.threadSleep(iSleepTime);
            iWait += iSleepTime;
            if (ans[0] != null) {
                break;
            }
        }
        return (ans[0] != null) && ((ans[0].equals(ansString)) || (ans[0].equals(ansString.replace("-", ""))));
    }

}
