package com.timeteccloud.smartlockble;

import com.zkteco.android.tool.ZKLog;

public class BioLockATConstants {
    private static final String TAG = "BioLockATConstants";
    private static final String PREFIX = "AT+";
    private static final String SUFFIX = "\r\n";

    public static final String RESET = PREFIX + "RESET" + SUFFIX;
    public static final String REFAC = PREFIX + "REFAC" + SUFFIX;

    public static final String ANS_PASSKEY_TRUE = "-PASSKEY TRUE" + SUFFIX;
    public static final String ANS_OK = "-OK" + SUFFIX;

    // 获取蓝牙模块固件版本
    public static final String FVERSION = PREFIX + "VERSION" + SUFFIX;
    public static final String ANS_VERSION = "+VERSION";

    public static String getVerifyPinString(String pin) {
        return PREFIX + "PASSKEY=" + pin + SUFFIX;
    }

    public static String getChangeNameString(String name) {
        return PREFIX + "NAME=" + name + SUFFIX;
    }

    public static String getChangePinString(String pin) {
        return PREFIX + "TK=" + pin + SUFFIX;
    }


    public static byte[] getLongNameChangeString(String name) {
        if ((name.getBytes().length + 2) % 12 == 1) {
            name += " ";     //防止/r/n被切割开，导致失败
        }
        name = name + SUFFIX;
        byte[] data = name.getBytes();
        byte[] ans = new byte[100];
        byte[] fix = (PREFIX + "NAME=").getBytes();

        int position = 0, dataPosition = 0;
        while (dataPosition < data.length) {
            //AT+NAME=
            if (position % 20 == 0) {
                for (int i = 0; i < 8; i++) {
                    ans[position] = fix[i];
//                    ZKLog.d(TAG, "getLongNameChangeString: " + new String(ans));
                    position++;
                }
                continue;
            }
            //NAME 内容
            ans[position] = data[dataPosition];
//            ZKLog.d(TAG, "getLongNameChangeString: " + new String(ans));
            position++;
            dataPosition++;
        }
        byte[] output = new byte[position];
        System.arraycopy(ans, 0, output, 0, output.length);
        ZKLog.d(TAG, "getLongNameChangeString: ans=" + new String(ans));
        return output;
    }
}
