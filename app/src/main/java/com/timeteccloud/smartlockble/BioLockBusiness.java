package com.timeteccloud.smartlockble;

import com.zkteco.android.bluetooth.device.ZkBtDeviceType;
import com.zkteco.android.tool.ZKTool;

/**
 * Created by TimeTec on 30-Nov-17.
 */

public class BioLockBusiness {
    private static final String TAG = "BioLockBusiness";

    //*************************  pull 逻辑    **********************//
    public static boolean pullConnect(String password, boolean isOneKeyOpen) {
        boolean isSucceed = false;
        //线程等待,否者pull连接不上
        //等待设备唤醒
        ZKTool.threadSleep(100);

        //默认密码连接
        long pullHandle = BioLockService.connect(password);
        if (pullHandle == 0) return false;

        isSucceed = true;

        return isSucceed;
    }

    public static int openDoor(long pullHandle) {
        int ret = BioLockService.openDoor(pullHandle);
        if ((ret != 0)) {
            ret = BioLockService.openDoor(pullHandle);
        }
        return ret;
    }
}
