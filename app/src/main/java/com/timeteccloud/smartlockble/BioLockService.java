package com.timeteccloud.smartlockble;

import com.zkteco.android.bluetooth.ZKBluetoothFactory;
import com.zkteco.android.pullsdk.PullController;
import com.zkteco.android.pullsdk.PullHelper;
import com.zkteco.android.tool.ZKLog;

/**
 * Created by TimeTec on 30-Nov-17.
 */

public class BioLockService {
    private static final String TAG = BioLockService.class.getSimpleName();

    private static final int DEFAULT_BUFFER_LENGTH = 4096;
    private static final int DEFAULT_ARRAY_UNIT = 32;

    private static final String TABLE_USER = "user";
    private static final String TABLE_USER_AUTHORIZE = "userauthorize";
    private static final String TABLE_TRANSACTION = "transaction";
    private static final String TABLE_TIME_ZONE = "timezone";

    private static final String DEFAULT_FIELD_NAME = "*";
    private static final String DEFAULT_PULL_PASSWORD = "12312124234";

    public static long connect(String password) {
        cleanPullData();
        String bluetoothAddress = ZKBluetoothFactory.getZKBluetoothManager().getBluetoothAddress();
//        ZKLog.d(TAG, "bluetoothAddress is  " + bluetoothAddress);
        if (password == null || password.trim().length() == 0) {
            password = DEFAULT_PULL_PASSWORD;
        }
        return PullController.connectPullByBluetooth(password + "\n" +
                bluetoothAddress + "-" + (-1) + "\n", 2000);
    }

    public static int openDoor(long handle) {
        cleanPullData();
        //一键开门的步骤
        //验证完用户权限后
        //拉取表结构
        //同步时间
        ZKLog.d(TAG, "openDoor    handle is " + handle);
        return PullHelper.controlDevice(handle, 1, 1, 0, 5, 0, "");
    }

    private static void cleanPullData() {
        ZKBluetoothFactory.getBluetoothManagerBLE().cleanPullDataBuffer();
    }
}
