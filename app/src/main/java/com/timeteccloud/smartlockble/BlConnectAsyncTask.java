package com.timeteccloud.smartlockble;

import android.content.Context;
import android.util.Log;

import com.zkteco.android.bluetooth.ZKBluetoothFactory;
import com.zkteco.android.bluetooth.ble.BluetoothManagerBLE;
import com.zkteco.android.bluetooth.device.ZKBluetoothDevice;
import com.zkteco.android.pullsdk.PullController;

public class BlConnectAsyncTask extends BaseAsyncTask<Void, String, Boolean> {
    private static final String TAG = "BlConnectAsyncTask";
    private static final int ERROR_PAIR_FAILED = -21;
    private ZKBluetoothDevice zkBluetoothDevice;
    private BluetoothManagerBLE bluetoothManagerBLE;
    private String mPairPwd = "000000";
    private boolean isOneKeyOpen = false;
    private boolean isOpenDoor = false;
    private boolean isNeedToast = false;
    private int ret = 2333;

    public BlConnectAsyncTask(Context context,ZKBluetoothDevice zkBluetoothDevice, String pairPwd){
        super(context);
        this.zkBluetoothDevice = zkBluetoothDevice;
        bluetoothManagerBLE = ZKBluetoothFactory.getBluetoothManagerBLE();
        mPairPwd = pairPwd;
    }

    public BlConnectAsyncTask(Context context, ZKBluetoothDevice zkBluetoothDevice, String pairPwd, boolean isOneKeyOpen, boolean isOpenDoor){
        this(context, zkBluetoothDevice, pairPwd);
        this.isOneKeyOpen = isOneKeyOpen;
        this.isOpenDoor = isOpenDoor;
        this.bluetoothManagerBLE = ZKBluetoothFactory.getBluetoothManagerBLE();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        if(!bluetoothManagerBLE.connectBlock(zkBluetoothDevice)){
            return false;
        }

        if(BioBtATController.pair(mPairPwd)){
            Log.i(TAG, "Pairing SUCCESS!");
        }else{
            bluetoothManagerBLE.disconnect();
            Log.i(TAG, "Pairing FAILED!");
        }


        if(!BioLockBusiness.pullConnect("00000000", isOneKeyOpen)){
            return false;
        }

        isOpenDoor = true;

        if(isOpenDoor){
            ret = BioLockBusiness.openDoor(PullController.getPullHandle());
            bluetoothManagerBLE.disconnect();
        }

        return true;
    }


}
