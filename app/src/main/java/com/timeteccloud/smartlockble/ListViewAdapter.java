package com.timeteccloud.smartlockble;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.zkteco.android.bluetooth.device.ZKBluetoothDevice;

import java.util.ArrayList;

/**
 * Created by TimeTec on 30-Nov-17.
 */

public class ListViewAdapter extends ArrayAdapter<ZKBluetoothDevice> {

//    ArrayList<ZKBluetoothDevice> zkBluetoothDevices;
    Context context;

    public ListViewAdapter(Context context, ArrayList<ZKBluetoothDevice> zkBluetoothDevices){
        super(context, 0, zkBluetoothDevices);
        this.context = context.getApplicationContext();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ZKBluetoothDevice ble = getItem(position);

        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_ble, parent, false);
        }

        TextView a = convertView.findViewById(R.id.ble_device_name);
        a.setText(ble.getName());

        return convertView;
    }

}
