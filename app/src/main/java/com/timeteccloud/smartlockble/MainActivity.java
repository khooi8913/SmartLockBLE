package com.timeteccloud.smartlockble;

import android.Manifest;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.zkteco.android.bluetooth.ZKBluetoothFactory;
import com.zkteco.android.bluetooth.ble.BioBTGattAttributes;
import com.zkteco.android.bluetooth.ble.BluetoothManagerBLE;
import com.zkteco.android.bluetooth.device.ZKBluetoothDevice;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private BluetoothManagerBLE bluetoothManagerBLE;
    private ZKBluetoothDevice zkBluetoothDevice;

    private ListView listView;
    private ListViewAdapter listAdapter;

    private ArrayList<ZKBluetoothDevice> listViewData;
    private HashMap<String, ZKBluetoothDevice> listViewMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

    }

    @Override
    protected void onStart() {
        super.onStart();
        listViewData = new ArrayList<>();
        listViewMap = new HashMap<>();

        ZKBluetoothFactory.generateBluetoothManager(getApplicationContext(), 40);

        bluetoothManagerBLE = ZKBluetoothFactory.getBluetoothManagerBLE();
        bluetoothManagerBLE.addConnectionLostListener(connectionLostListener);

        listView = findViewById(R.id.listView);
        listAdapter = new ListViewAdapter(getApplicationContext(), listViewData);
        listView.setAdapter(listAdapter);

        listAdapter.notifyDataSetChanged();


        startScan();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                connectLock("000000", listAdapter.getItem(position));
            }
        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                long startTime = System.currentTimeMillis();
//
//                Log.i("TIME", (System.currentTimeMillis()-startTime)+"ms");
//            }
//        }, 5000);
    }

    private BluetoothManagerBLE.ConnectionLostListener connectionLostListener = new BluetoothManagerBLE.ConnectionLostListener() {
        @Override
        public void onConnectLost() {
            //TODO: Connection lost
        }
    };

    ZKBluetoothDevice temp;

    private void startScan() {
        Log.i("Notification","Scan Starting.");
        bluetoothManagerBLE.scanLe(true, BioBTGattAttributes.SERVICE_BIO_BT, new BluetoothManagerBLE.ConnScanCallback() {
            @Override
            public void deviceFind(ZKBluetoothDevice device) {
                Log.i("ScanResults_Name", device.getName());
                Log.i("ScanResults_Address", device.getAddress());
                temp = device;
                if(!listViewMap.containsKey(device.getAddress())){
                    listViewData.add(device);
                    listViewMap.put(device.getAddress(), device);
                    listAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void scanStop() {
                Log.i("Notification","Scan Stopping.");
                Toast.makeText(getApplicationContext(), "Scanning Done!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void connectLock(String pwd, ZKBluetoothDevice zkBluetoothDevice){
        new BlConnectAsyncTask(getApplicationContext(), zkBluetoothDevice, pwd){
            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
            }
        }.execute();
    }
}
